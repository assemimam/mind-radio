//
//  AppDelegate.m
//  SAUDI_KAU
//
//  Created by assem on 7/21/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "AppDelegate.h"
#import  "Language.h"
#import <AudioToolbox/AudioToolbox.h>
#import "MenuViewController.h"
#import "MFSideMenuContainerViewController.h"
@implementation AppDelegate
#pragma -mark uilocal notifications delegate
-(void)HideNotificationView
{
    @try {
        [UIView animateWithDuration:0.5 animations:^{
           
            [UIView animateWithDuration:0.5 animations:^{
                [NotificationView setFrame:CGRectMake(0, -1*NotificationView.frame.size.height, NotificationView.frame.size.width, NotificationView.frame.size.height)];
            } completion:^(BOOL finished) {
                if (finished) {
                     [NotificationView setHidden:YES];
                    [self.window sendSubviewToBack:NotificationView];
                }
            }];
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)ShowNotificationView
{
    @try {

        NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"notify" ofType:@"mp3"];
        SystemSoundID soundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
        AudioServicesPlaySystemSound (soundID);
        
        [NotificationView setHidden:NO];
        [self.window bringSubviewToFront:NotificationView];
        [UIView animateWithDuration:0.5 animations:^{
            [NotificationView setFrame:CGRectMake(0, 0, NotificationView.frame.size.width, NotificationView.frame.size.height)];
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    @try {
         id show = notification.userInfo;
        if (show) {
            NSDateFormatter *dateTimeFormatter =[[NSDateFormatter alloc]init];
            [dateTimeFormatter setDateFormat:@"dd/MM/yyyy  HH:mm:ss"];
            dateTimeFormatter.timeZone=[NSTimeZone localTimeZone];
            dateTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            NSDate *showFiredDate= [dateTimeFormatter dateFromString:[show objectForKey:@"FiredDate"]];
            NSString *currentDateString = [dateTimeFormatter stringFromDate:[NSDate date]];
            NSDate *currentDate = [dateTimeFormatter dateFromString:currentDateString];
            
            ShowNameLabel.text = [show objectForKey:@"Title"];
            if ([currentDate compare:showFiredDate]==NSOrderedSame)  {
                [self ShowNotificationView];
                [self performSelector:@selector(HideNotificationView) withObject:nil afterDelay:5.0];
            }
            
            [[Cashing getObject] setDatabase:@"mind_radio"];
            DB_Field  *fldShowId = [[DB_Field alloc]init];
            fldShowId.FieldName=@"ShowId";
            fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldShowId.IS_PRIMARY_KEY=YES;
            fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
            
            DB_Field  *fldShowDate= [[DB_Field alloc]init];
            fldShowDate.FieldDataType =FIELD_DATA_TYPE_TEXT;
            fldShowDate.FieldName=@"ShowDate";
            fldShowDate.FieldValue=[show objectForKey:@"ShowDate"];
            
            DB_Field  *fldShowTime= [[DB_Field alloc]init];
            fldShowTime.FieldName=@"ShowTime";
            fldShowTime.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldShowTime.FieldValue=[show objectForKey:@"ShowTime"];
            
            DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
            fldIsWeekly.FieldName=@"IsWeekly";
            fldIsWeekly.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldIsWeekly.FieldValue=[show objectForKey:@"IsWeekly"];
            
            DB_Field  *fldInsertDate= [[DB_Field alloc]init];
            fldInsertDate.FieldName=@"InsertDate";
            fldInsertDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldInsertDate.FieldValue=[show objectForKey:@"InsertDate"];
            
            DB_Field  *fldFired= [[DB_Field alloc]init];
            fldFired.FieldName=@"Fired";
            fldFired.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldFired.FieldValue=@"1";
            
            DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
            showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldShowTime,fldInsertDate,fldIsWeekly,fldShowDate,fldFired, nil];
            
            BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"my_alerts"];
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(int)GetWeekDayFromDayName:(NSString*)dayName
{
    int weekDayNo;
    
    if ([dayName isEqualToString:@"Sunday"]) {
        weekDayNo=1;
    }
    else if ([dayName isEqualToString:@"Monday"])
    {
         weekDayNo=2;
    }
    else if ([dayName isEqualToString:@"Tuesday"])
    {
        weekDayNo=3;
    }
    else if ([dayName isEqualToString:@"Wednesday"])
    {
        weekDayNo=4;
    }
    else if ([dayName isEqualToString:@"Thursday"])
    {
        weekDayNo=5;
    }
    else if ([dayName isEqualToString:@"Friday"])
    {
        weekDayNo=6;
    }
    else if ([dayName isEqualToString:@"Saturday"])
    {
        weekDayNo=7;
    }
    return weekDayNo;
}
-(int)GetWeekDayFromDate:(NSDate*)date
{
    NSDateComponents* comp;
    @try {
        NSCalendar* cal = [NSCalendar currentCalendar];
        comp = [cal components:NSWeekdayCalendarUnit fromDate:date];
    }
    @catch (NSException *exception) {
        
    }
    return [comp weekday];
}
-(void)ScheduleShowAlerts
{
    @try {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        NSDateFormatter *dateTimeFormatter =[[NSDateFormatter alloc]init];
        [dateTimeFormatter setDateFormat:@"dd/MM/yyyy  HH:mm:ss"];
        dateTimeFormatter.timeZone=[NSTimeZone localTimeZone];
        dateTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        dateFormatter.timeZone=[NSTimeZone localTimeZone];
        dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSMutableArray *schedualedShows = [[NSMutableArray alloc]init];
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"my_alerts.ShowId";
        fldShowId.FieldAlias=@"ShowId";
        
        DB_Field  *fldShowDate= [[DB_Field alloc]init];
        fldShowDate.FieldName=@"ShowDate";
        
        DB_Field  *fldDayOfWeek= [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
        
        DB_Field  *fldShowTime= [[DB_Field alloc]init];
        fldShowTime.FieldName=@"ShowTime";
        
        DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
        fldIsWeekly.FieldName=@"IsWeekly";
        
        DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
        fldThumbNailURL.FieldName=@"ThumbNailURL";
        
        DB_Field  *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        DB_Field  *fldFired= [[DB_Field alloc]init];
        fldFired.FieldName=@"Fired";
        
        DB_Field  *fldInsertDate= [[DB_Field alloc]init];
        fldInsertDate.FieldName=@"InsertDate";
        
        id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldShowTime,fldDayOfWeek,fldInsertDate,fldThumbNailURL,fldTitle,fldShowDate,fldIsWeekly,fldFired,nil] Tables:[NSArray arrayWithObjects:@"my_alerts",@"shows", nil] Where:@"my_alerts.ShowId = shows.ShowId" FromIndex:nil ToIndex:nil OrderByField:@"InsertDate" AscendingOrder:NO];
        if (showsResut) {
            if ([showsResut count]>0) {
                for (id show  in showsResut) {
                    
                    NSMutableDictionary*showDic = [[NSMutableDictionary alloc]init];
                    [showDic setObject:[show objectForKey:@"Title"] forKey:@"Title"];
                    [showDic setObject:[show objectForKey:@"IsWeekly"] forKey:@"IsWeekly"];
                    [showDic setObject:[show objectForKey:@"ShowId"] forKey:@"ShowId"];
                    [showDic setObject:[show objectForKey:@"ShowTime"] forKey:@"ShowTime"];
                    [showDic setObject:[show objectForKey:@"ShowDate"] forKey:@"ShowDate"];
                    [showDic setObject:[show objectForKey:@"InsertDate"] forKey:@"InsertDate"];
                    [showDic setObject:[show objectForKey:@"Fired"] forKey:@"Fired"];
                    
                    int showDayNumber = [self GetWeekDayFromDayName:[show objectForKey:@"DayOfWeek"]];
                    int currentDayNumber = [self GetWeekDayFromDate:[NSDate date]];
        
                    NSString *showDateString = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:[NSDate date]],[show objectForKey:@"ShowTime"]];
                    NSDate *showDate = [dateTimeFormatter dateFromString:showDateString];
                    NSDate *showFiredDate;
                    if (showDayNumber>=currentDayNumber) {
                        showFiredDate= [showDate dateByAddingTimeInterval:(showDayNumber - currentDayNumber)*24*60*60];
                    }
                    else{
                        showFiredDate= [showDate dateByAddingTimeInterval:(7-abs((showDayNumber - currentDayNumber)))*24*60*60];
                    }

                    [showDic setObject:[dateTimeFormatter stringFromDate:showFiredDate] forKey:@"FiredDate"];
                    
                    if ([[show objectForKey:@"IsWeekly"]intValue]==1) {
                        if ([showFiredDate compare:[NSDate date]]!=NSOrderedAscending) {
                              [schedualedShows addObject:showDic];
                        }
                    }
                    else{
                        if ([[show objectForKey:@"Fired"]intValue]==0) {
                              if ([showFiredDate compare:[NSDate date]]!=NSOrderedAscending)  {
                               [schedualedShows addObject:showDic];
                             }
                        }
                    }
                }
            }
        }
        
        if ([schedualedShows count]>0) {
            for (id show in schedualedShows) {
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [dateTimeFormatter dateFromString:[show objectForKey:@"FiredDate"]];
                localNotification.alertBody = [NSString stringWithFormat:@"%@", [show objectForKey:@"Title"]];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                if ([[show objectForKey:@"IsWeekly"]intValue]==1) {
                    localNotification.repeatInterval =kCFCalendarUnitWeek;
                }
                else{
                    localNotification.repeatInterval =0;
                }
        
                localNotification.userInfo = show;
                localNotification.applicationIconBadgeNumber = 0;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (BOOL)validNetworkConnection
{
    
    @try {
        struct sockaddr_in zeroAddress;
        bzero(&zeroAddress, sizeof(zeroAddress));
        zeroAddress.sin_len = sizeof(zeroAddress);
        zeroAddress.sin_family = AF_INET;
        
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
        
        SCNetworkReachabilityFlags flags = 0;
        Boolean bFlagsValid = SCNetworkReachabilityGetFlags(reachability, &flags);
        CFRelease(reachability);
        
        if (!bFlagsValid)
            return NO;
        
        if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
        {
            // if target host is not reachable
            return NO;//NotReachable;
        }
        
        BOOL retVal = NO;
        
        if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
        {
            // if target host is reachable and no connection is required
            //  then we'll assume (for now) that your on Wi-Fi
            retVal = YES;
        }
        
        
        if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
             (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
        {
            // ... and the connection is on-demand (or on-traffic) if the
            //     calling application is using the CFSocketStream or higher APIs
            
            if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
            {
                // ... and no [user] intervention is needed
                retVal = YES;
            }
        }
        
        if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
        {
            // ... but WWAN connections are OK if the calling application
            //     is using the CFNetwork (CFSocketStream?) APIs.
            retVal = YES;
        }
        
        return retVal;
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleDeletedAndModifiedShows:(id)result
{
    @try {
        //handle deleted shows
        [[Cashing getObject] setDatabase:@"mind_radio"];
        NSString *accomulatedShowIDString=@"";
    
        for (id show in [result objectForKey:@"Saturday"]) {
                accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Sunday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Monday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Tuseday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Wednesday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Thursday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        for (id show in [result objectForKey:@"Friday"]) {
            accomulatedShowIDString= [accomulatedShowIDString stringByAppendingFormat:@"%i,",[[show objectForKey:@"ShowId"]intValue]];
        }
        
        accomulatedShowIDString = [accomulatedShowIDString substringToIndex:[accomulatedShowIDString length] - 1];
        if ([accomulatedShowIDString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length >0) {
           [[Cashing getObject]DeleteDataFromTable:@"shows" WhereCondition:[NSString stringWithFormat:@"ShowId not in (%@)",accomulatedShowIDString]];
           [[Cashing getObject]DeleteDataFromTable:@"show_hosts" WhereCondition:[NSString stringWithFormat:@"ShowId not in (%@)",accomulatedShowIDString]];
           BOOL isSuccess =[[Cashing getObject]DeleteDataFromTable:@"my_alerts" WhereCondition:[NSString stringWithFormat:@"ShowId not in (%@)",accomulatedShowIDString]];
            if (isSuccess) {
                [self ScheduleShowAlerts];
            }
        }
        
       
        //handle modified time shows.
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"my_alerts.ShowId";
        fldShowId.FieldAlias=@"AlertShowId";
        
        DB_Field  *fldShowDate= [[DB_Field alloc]init];
        fldShowDate.FieldName=@"ShowDate";
        
        DB_Field  *fldShowTime= [[DB_Field alloc]init];
        fldShowTime.FieldName=@"ShowTime";
        
        DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
        fldIsWeekly.FieldName=@"IsWeekly";
        
        DB_Field  *fldFired= [[DB_Field alloc]init];
        fldFired.FieldName=@"Fired";
        
        DB_Field  *fldInsertDate= [[DB_Field alloc]init];
        fldInsertDate.FieldName=@"InsertDate";
        
        DB_Field  *fldLastTimeZoon = [[DB_Field alloc]init];
        fldLastTimeZoon.FieldName=@"LastTimeZoon";
        
        DB_Field  *fldRemindBefore = [[DB_Field alloc]init];
        fldRemindBefore.FieldName=@"RemindBefore";
        
        DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
        fldStartTimeStr.FieldName=@"StartTimeStr";
        
        DB_Field  *fldTimeZone = [[DB_Field alloc]init];
        fldTimeZone.FieldName=@"TimeZone";
        
        DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
 
        id alertResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldShowDate,fldShowTime,fldFired,fldIsWeekly,fldLastTimeZoon,fldInsertDate,fldRemindBefore,fldStartTimeStr,fldTimeZone,fldDayOfWeek,nil] Tables:[NSArray arrayWithObjects:@"my_alerts",@"shows", nil] Where:@"shows.ShowId = my_alerts.ShowId"  FromIndex:nil ToIndex:nil OrderByField:@"InsertDate" AscendingOrder:NO];
        
        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
        [timeFormatter setDateFormat:@"HH:mm:ss"];
        timeFormatter.timeZone=[NSTimeZone localTimeZone];
        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        if ([alertResut count]>0) {
            for (id alert in alertResut) {
                NSDate *alertShowDate =[timeFormatter dateFromString:[alert objectForKey:@"ShowDate"]];
                NSDate *showDate =[timeFormatter dateFromString:[alert objectForKey:@"StartTimeStr"]];
               
                NSTimeInterval secondsBetweenTimeInterval = [showDate timeIntervalSinceDate:alertShowDate];
                if ((int)secondsBetweenTimeInterval!=0) {
                    NSDate *alertShowTime =  [[alertShowDate dateByAddingTimeInterval:secondsBetweenTimeInterval]dateByAddingTimeInterval:-1*([[alert objectForKey:@"RemindBefore"]intValue] * 60) ];
                   alertShowDate=  [alertShowDate dateByAddingTimeInterval:secondsBetweenTimeInterval];
                    
                    int  timeZoonOffset =[NSNull null]!=[alert objectForKey:@"TimeZone"]?[[alert objectForKey:@"TimeZone"]intValue]:0;
                    NSString * convertedAlertShowDate = [self ConvertToDeviceTime:[timeFormatter stringFromDate:alertShowDate] AndTimeOffset:timeZoonOffset];
                    NSString * convertedAlertShowTime = [self ConvertToDeviceTime:[timeFormatter stringFromDate:alertShowTime] AndTimeOffset:timeZoonOffset];

                    DB_Field  *fldLastTimeZoon = [[DB_Field alloc]init];
                    fldLastTimeZoon.FieldName=@"LastTimeZoon";
                    fldLastTimeZoon.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldLastTimeZoon.FieldValue =[alert objectForKey:@"LastTimeZoon"];
                    
                    DB_Field  *fldShowId = [[DB_Field alloc]init];
                    fldShowId.FieldName=@"ShowId";
                    fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldShowId.IS_PRIMARY_KEY=YES;
                    fldShowId.FieldValue= [alert objectForKey:@"AlertShowId"];
                    
                    DB_Field  *fldShowDate= [[DB_Field alloc]init];
                    fldShowDate.FieldDataType =FIELD_DATA_TYPE_TEXT;
                    fldShowDate.FieldName=@"ShowDate";
                    fldShowDate.FieldValue=convertedAlertShowDate;
                    
                    DB_Field  *fldShowTime= [[DB_Field alloc]init];
                    fldShowTime.FieldName=@"ShowTime";
                    fldShowTime.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldShowTime.FieldValue=convertedAlertShowTime;
                    
                    DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
                    fldIsWeekly.FieldName=@"IsWeekly";
                    fldIsWeekly.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldIsWeekly.FieldValue=[alert objectForKey:@"IsWeekly"];
                    
        
                    DB_Field  *fldInsertDate= [[DB_Field alloc]init];
                    fldInsertDate.FieldName=@"InsertDate";
                    fldInsertDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldInsertDate.FieldValue=[alert objectForKey:@"InsertDate"];
                    
                    DB_Field  *fldFired= [[DB_Field alloc]init];
                    fldFired.FieldName=@"Fired";
                    fldFired.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldFired.FieldValue=[alert objectForKey:@"Fired"];
                    
                    DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                    showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldShowTime,fldInsertDate,fldIsWeekly,fldShowDate,fldFired,fldLastTimeZoon, nil];
                    
                    BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"my_alerts"];
                    if (isSuccess) {
                        [self ScheduleShowAlerts];
                    }

                }
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleChangeTimeZoonForAlerts
{
    @try {
        BOOL changeExist = NO;
        [[Cashing getObject] setDatabase:@"mind_radio"];
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";
        
        DB_Field  *fldShowDate= [[DB_Field alloc]init];
        fldShowDate.FieldName=@"ShowDate";

        DB_Field  *fldShowTime= [[DB_Field alloc]init];
        fldShowTime.FieldName=@"ShowTime";
        
        DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
        fldIsWeekly.FieldName=@"IsWeekly";

        DB_Field  *fldFired= [[DB_Field alloc]init];
        fldFired.FieldName=@"Fired";
        
        DB_Field  *fldInsertDate= [[DB_Field alloc]init];
        fldInsertDate.FieldName=@"InsertDate";
        
        DB_Field  *fldLastTimeZoon = [[DB_Field alloc]init];
        fldLastTimeZoon.FieldName=@"LastTimeZoon";

        id alertResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldShowDate,fldShowTime,fldFired,fldIsWeekly,fldLastTimeZoon,fldInsertDate,nil] Tables:[NSArray arrayWithObjects:@"my_alerts", nil] Where:nil  FromIndex:nil ToIndex:nil OrderByField:@"InsertDate" AscendingOrder:NO];
        
        if ([alertResut count]>0) {
            for (id alert in alertResut) {
                if ([[alert objectForKey:@"LastTimeZoon"]floatValue] != [[NSString stringWithFormat:@"%0.1f",(float)[[NSTimeZone systemTimeZone] secondsFromGMT]/3600.0]floatValue]) {
                   
                    changeExist =YES;
                    
                    NSString *showTime;
                    float  timeZoonOffset =[NSNull null]!=[ alert objectForKey:@"LastTimeZoon"]?[[alert objectForKey:@"LastTimeZoon"]floatValue]:0.0f;
                    float timeZoneSeconds=timeZoonOffset*3600;
                    
                    NSTimeZone* deviceTimeZone = [NSTimeZone localTimeZone];
                    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:timeZoneSeconds];
                    NSDateFormatter *serverTimeFormatter =[[NSDateFormatter alloc]init];
                    [serverTimeFormatter setDateFormat:@"HH:mm:ss"];
                    serverTimeFormatter.timeZone=sourceTimeZone;
                    serverTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
                    NSDate *serverShowTime =[serverTimeFormatter dateFromString:[alert objectForKey:@"ShowTime"]];
                    
                    NSInteger currentGMTOffset = [deviceTimeZone secondsFromGMTForDate:serverShowTime];
                    NSTimeZone *convertedSourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset];
                   
                    NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
                    [timeFormatter setDateFormat:@"HH:mm:ss"];
                    timeFormatter.timeZone=convertedSourceTimeZone;
                    timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
                    showTime =[timeFormatter stringFromDate:serverShowTime];

                    DB_Field  *fldLastTimeZoon = [[DB_Field alloc]init];
                    fldLastTimeZoon.FieldName=@"LastTimeZoon";
                    fldLastTimeZoon.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldLastTimeZoon.FieldValue =[NSString stringWithFormat:@"%0.1f",(float)[[NSTimeZone systemTimeZone] secondsFromGMT]/3600.0];
                    
                    DB_Field  *fldShowId = [[DB_Field alloc]init];
                    fldShowId.FieldName=@"ShowId";
                    fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldShowId.IS_PRIMARY_KEY=YES;
                    fldShowId.FieldValue= [alert objectForKey:@"ShowId"];
                    
                    DB_Field  *fldShowDate= [[DB_Field alloc]init];
                    fldShowDate.FieldDataType =FIELD_DATA_TYPE_TEXT;
                    fldShowDate.FieldName=@"ShowDate";
                    fldShowDate.FieldValue=[alert objectForKey:@"ShowDate"];
                    
                    DB_Field  *fldShowTime= [[DB_Field alloc]init];
                    fldShowTime.FieldName=@"ShowTime";
                    fldShowTime.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldShowTime.FieldValue=showTime;
                    
                    DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
                    fldIsWeekly.FieldName=@"IsWeekly";
                    fldIsWeekly.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldIsWeekly.FieldValue=[alert objectForKey:@"IsWeekly"];
                    
                    DB_Field  *fldInsertDate= [[DB_Field alloc]init];
                    fldInsertDate.FieldName=@"InsertDate";
                    fldInsertDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
                    fldInsertDate.FieldValue=[alert objectForKey:@"InsertDate"];
                    
                    DB_Field  *fldFired= [[DB_Field alloc]init];
                    fldFired.FieldName=@"Fired";
                    fldFired.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                    fldFired.FieldValue=[alert objectForKey:@"Fired"];
                    
                    DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                    showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldShowTime,fldInsertDate,fldIsWeekly,fldShowDate,fldFired,fldLastTimeZoon, nil];
                    
                    BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"my_alerts"];

                }
            }
            if (changeExist) {
                [self ScheduleShowAlerts];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)DidChangeTime
{
    @try {
    }
    @catch (NSException *exception) {
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DidChangeTime) name:UIApplicationSignificantTimeChangeNotification object:nil];
    InternetConnection = [self validNetworkConnection];
  
     //[self ScheduleShowAlerts];
    [[Language getObject] setLanguage:languageEnglish];
    MenuViewController *vc_menu = [[MenuViewController alloc]initWithNibName:@"MenuViewController" bundle:nil];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:self.NavigationContoller
                                                    leftMenuViewController:vc_menu
                                                    rightMenuViewController:nil];
    self.window.rootViewController = container;
    if (InternetConnection) {
        [NSThread detachNewThreadSelector:@selector(GetShowsList) toTarget:self withObject:nil];
    }

    ////////////////////////////////////////////////
    //    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    //    NSArray *fontNames;
    //    NSInteger indFamily, indFont;
    //    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    //    {
    //        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
    //        fontNames = [[NSArray alloc] initWithArray:
    //                     [UIFont fontNamesForFamilyName:
    //                      [familyNames objectAtIndex:indFamily]]];
    //        for (indFont=0; indFont<[fontNames count]; ++indFont)
    //        {
    //            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
    //        }
    //
    //    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(NSMutableArray*)GetExistanceIndexesOfTheShow:(NSString*)show_name InShowList:(NSArray*)showList ForKey:(NSString*)key
{
    NSMutableArray* returnIndexes=[[NSMutableArray alloc]init];
    @try {
        int objectIndex = 0;
        int index=0;
        
        for (id show in showList) {
            if ([[[show objectForKey:key] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] caseInsensitiveCompare:[show_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]==NSOrderedSame)
            {
                if (index!=0) {
                    [returnIndexes addObject:[NSString stringWithFormat:@"%i",objectIndex]];
                    break;
                }
                index++;
            }
            objectIndex ++;
        }
    }
    @catch (NSException *exception) {
        
    }
    return returnIndexes;
}
-(void)GetShowsList
{
    @try {
        id result = [Webservice GetShows];
        [self performSelectorOnMainThread:@selector(HandleGetShows:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)CashShows:(id)result
{
    @try {
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        if ([NSNull null]!= [result objectForKey:@"Saturday"]) {
            for (id show in [result objectForKey:@"Saturday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Saturday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Sunday"]) {
            for (id show in [result objectForKey:@"Sunday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Sunday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Monday"]) {
            for (id show in [result objectForKey:@"Monday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Monday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Tuseday"]) {
            for (id show in [result objectForKey:@"Tuseday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Tuseday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Wednesday"]) {
            for (id show in [result objectForKey:@"Wednesday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Wednesday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Thursday"]) {
            for (id show in [result objectForKey:@"Thursday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Thursday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Friday"]) {
            for (id show in [result objectForKey:@"Friday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Friday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        [self HandleDeletedAndModifiedShows:result];
        [self  HandleChangeTimeZoonForAlerts];
       
        
    }
    @catch (NSException *exception) {
        
    }
    
}



-(void)HandleGetShows:(id)result
{
    @try {
        if (result) {
            [NSThread detachNewThreadSelector:@selector(CashShows:) toTarget:self withObject:result];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

- (NSString *)ConvertToDeviceTime:(id)time AndTimeOffset:(float)timeOffset
{
    time =[NSNull null]!= time?time:@"";
    float timeZoneSeconds=timeOffset*3600.0f;
    
    NSTimeZone* deviceTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:timeZoneSeconds];
    NSDateFormatter *serverTimeFormatter =[[NSDateFormatter alloc]init];
    [serverTimeFormatter setDateFormat:@"HH:mm:ss"];
    serverTimeFormatter.timeZone=sourceTimeZone;
    serverTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    NSDate *serverShowTime =[serverTimeFormatter dateFromString:time];
    
    NSInteger currentGMTOffset = [deviceTimeZone secondsFromGMTForDate:serverShowTime];
    NSTimeZone *convertedSourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset];
    NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    timeFormatter.timeZone=convertedSourceTimeZone;
    timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    NSString *showTime =[timeFormatter stringFromDate:serverShowTime];
    return showTime;
}

@end
