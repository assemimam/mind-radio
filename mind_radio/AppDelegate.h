//
//  AppDelegate.h
//  SAUDI_KAU
//
//  Created by assem on 7/21/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    BOOL notificationFired;
    __weak IBOutlet UIView *NotificationView;
    __weak IBOutlet UILabel *ShowNameLabel;
}
@property (strong, nonatomic)IBOutlet UIWindow *window;
@property (weak, nonatomic) IBOutlet UINavigationController *NavigationContoller;
-(void)ScheduleShowAlerts;
-(int)GetWeekDayFromDayName:(NSString*)dayName;
-(int)GetWeekDayFromDate:(NSDate*)date;
- (BOOL)validNetworkConnection;
@end
