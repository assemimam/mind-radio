//
//  ShowCell.h
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ShowNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *HostsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ShowImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;

@end
