//
//  SchedualCell.h
//  SAUDI_KAU
//
//  Created by Assem Imam on 7/22/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface SchedualCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ProgramNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ProgramTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;
@property (weak, nonatomic) IBOutlet UILabel *SpeakersLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *ShowImageView;

@end
