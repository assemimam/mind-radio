//
//  ShowCell.m
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "ShowCell.h"

@implementation ShowCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
