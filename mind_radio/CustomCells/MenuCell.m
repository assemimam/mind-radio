//
//  MenuCell.m
//  mind_radio
//
//  Created by Assem Imam on 10/27/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
