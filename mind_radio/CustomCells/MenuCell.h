//
//  MenuCell.h
//  mind_radio
//
//  Created by Assem Imam on 10/27/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ItemTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *DayLabel;

@end
