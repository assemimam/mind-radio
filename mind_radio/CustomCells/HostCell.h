//
//  HostCell.h
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *BackgroundButton;
@property (weak, nonatomic) IBOutlet UIImageView *HostImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *HostNameLabel;

@end
