//
//  Cashing.m
//  AmChamb
//
//  Created by Assem Imam on 3/11/14.
//  Copyright (c) 2014 tawasol. All rights reserved.
//

#import "Cashing.h"
#import "Database.h"
@interface Cashing ()
{
    Database *db;
    
}
@end

Cashing *CashingObject = nil;
@implementation Cashing
@synthesize databaseName;

+ (Cashing *)getObject{
    @try {
        if (CashingObject == nil) {
            CashingObject = [[Cashing alloc] init];
        }
    }
    @catch (NSException *exception) {
        CashingObject=nil;
    }
   
	
	
	return CashingObject;
}

- (void)setDatabase:(NSString *)name{
    @try {
        self.databaseName = name;
        db = [Database getObject];
        [db setDatabase:name];
        [db CopyDataBaseFileToDocumentsDirectory];
    }
    @catch (NSException *exception) {
        
    }
}
-(BOOL)DeleteDataFromTable:(NSString*)table_name WhereCondition:(NSString*)where_condition
{
    BOOL result=NO;
    @try {
        result= [db DeleteFromTable:table_name Where:where_condition];
    }
    @catch (NSException *exception) {
        
    }
    return result;
}
-(BOOL)CashData:(id)data inTable:(NSString*)table_name
{
  
    BOOL Success = YES;
    @try {
       
            for (DB_Recored *recored in data) {
                DB_Field *primarkKeyField ;
                NSMutableArray*primaryKeyFields=[[NSMutableArray alloc]init];
                NSString *whereCondition = @"";
                NSString *primaryKeyFileName=@"";
                for (DB_Field *field in recored.Fields) {
                    if (field.IS_PRIMARY_KEY) {
                       [primaryKeyFields addObject:field];
                    }
                }
                if ([primaryKeyFields count]==0) {
                    return NO;
                }
                if ([primaryKeyFields count]==1) {
                     primarkKeyField=[primaryKeyFields objectAtIndex:0];
                    id value=@"";
                    switch (primarkKeyField.FieldDataType) {
                        case FIELD_DATA_TYPE_NUMBER:
                            value = [NSString stringWithFormat:@"%@",primarkKeyField.FieldValue];
                            break;
                        case FIELD_DATA_TYPE_TEXT:
                            value = [NSString stringWithFormat:@"'%@'",[primarkKeyField.FieldValue stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
                            break;
                        case FIELD_DATA_TYPE_DATE:
                            value = [NSString stringWithFormat:@"'%@'",primarkKeyField.FieldValue];
                            break;
                            
                            
                    }
                    
                    whereCondition=[NSString stringWithFormat:@"%@=%@",primarkKeyField.FieldName,value];
                    primaryKeyFileName=((DB_Field*)[primaryKeyFields objectAtIndex:0]).FieldValue;
                }
                else if ([primaryKeyFields count]>1) {
                    
                    for (DB_Field *field in primaryKeyFields) {
                        id value=@"";
                        switch (field.FieldDataType) {
                            case FIELD_DATA_TYPE_NUMBER:
                                value = [NSString stringWithFormat:@"%@",field.FieldValue];
                                break;
                            case FIELD_DATA_TYPE_TEXT:
                                value = [NSString stringWithFormat:@"'%@'",[field.FieldValue stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
                                break;
                            case FIELD_DATA_TYPE_DATE:
                                value = [NSString stringWithFormat:@"'%@'",field.FieldValue];
                                break;
                                
                                
                        }

                        primaryKeyFileName=[primaryKeyFileName stringByAppendingFormat:@"%@",field.FieldValue ];
                        whereCondition = [whereCondition stringByAppendingString:[NSString stringWithFormat:@"%@=%@ and ",field.FieldName,value]];
                        
                    }
                    whereCondition =[whereCondition substringToIndex:whereCondition.length-4];
                }
                for (DB_Field *field in recored.Fields) {

                    if (field.FieldDataType== FIELD_DATA_TYPE_IMAGE) {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *Documentpath = [paths objectAtIndex:0];
                        NSString *TargetPath = [Documentpath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@Images",table_name]];
                        TargetPath = [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@Images",field.FieldName]];
                        
                        NSFileManager *Fmanager = [NSFileManager defaultManager];
                        if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                            
                        }
                        else
                        {
                            BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                            if (ret) {
                                
                            }
                        }
                        NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",primaryKeyFileName]];
                        
                        NSURL *imageUrl = [NSURL URLWithString:field.FieldValue];
                        if (imageUrl!=nil) {
                            if ([imageUrl.absoluteString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "] ].length>0 ) {
                                dispatch_queue_t myQueue = dispatch_queue_create("com.mycompany.myqueue", 0);
                                dispatch_async(myQueue, ^{
                                    
                                    NSData *imageData = [[NSData alloc] initWithContentsOfURL:
                                                         imageUrl];
                                     [imageData writeToFile:fileName atomically:YES];
                                    
                                });

                                    field.FieldValue= fileName;
                                    field.FieldDataType= FIELD_DATA_TYPE_TEXT;

                            }
                            else {
                                field.FieldValue= @"";
                                field.FieldDataType= FIELD_DATA_TYPE_TEXT;
                            }
                            
                        }
                        else {
                            field.FieldValue= @"";
                            field.FieldDataType= FIELD_DATA_TYPE_TEXT;
                        }
                        
                    }
                    if (field.FieldDataType== FIELD_DATA_TYPE_PDF_FILE) {
                        
                        NSURL *pdfUrl = [NSURL URLWithString:field.FieldValue] ;
                        if (pdfUrl!=nil) {
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *Documentpath = [paths objectAtIndex:0];
                            NSString *TargetPath = [Documentpath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@PDF_Files",table_name]];
                            TargetPath = [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@PDF_Files",field.FieldName]];
                            
                            NSFileManager *Fmanager = [NSFileManager defaultManager];
                            if ([Fmanager fileExistsAtPath:TargetPath isDirectory:nil]) {
                                
                            }
                            else
                            {
                                BOOL ret =[Fmanager createDirectoryAtPath:TargetPath withIntermediateDirectories:YES attributes:nil error:nil];
                                if (ret) {
                                    
                                }
                            }
                            NSArray *fileExtensions =[field.FieldValue componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
                            NSString *extesion=@"";
                            if ([fileExtensions count ]>0) {
                                extesion =[fileExtensions lastObject];
                            }
                            
                            NSString *fileName= [TargetPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",primaryKeyFileName,extesion]];
                            
                            if ([pdfUrl.absoluteString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "] ].length>0 ) {
                                dispatch_queue_t myQueue = dispatch_queue_create("com.mycompany.myqueue", 0);
                                dispatch_async(myQueue, ^{
                                    NSData *pdfData = [[NSData alloc] initWithContentsOfURL:
                                                       pdfUrl];
                                    [pdfData writeToFile:fileName atomically:YES];

                                });
                                
                                    
                              field.FieldValue= fileName;
                              field.FieldDataType= FIELD_DATA_TYPE_TEXT;
  
                            }
                            else {
                                field.FieldValue= @"";
                                field.FieldDataType= FIELD_DATA_TYPE_TEXT;
                            }
                        }
                        else {
                            field.FieldValue= @"";
                            field.FieldDataType= FIELD_DATA_TYPE_TEXT;
                        }
                        
                    }
                    
                  
                }

                id result= [db selectColumns:[NSArray arrayWithObject:[NSString stringWithFormat:@"count('%@')",primarkKeyField.FieldName]] fromTable:table_name where:whereCondition andOrderBy:nil inAscendingOrder:YES withLimit:nil andOffect:nil];
                if (result) {
                    if ([result count]>0) {
                        int rowCount=[[[result objectAtIndex:0]objectForKey:[NSString stringWithFormat:@"count('%@')",primarkKeyField.FieldName]]intValue];
                        if (rowCount>0) {
                            Success=[db UpdateValues:recored.Fields  InTable:table_name Where:whereCondition];
                        }
                        else {// insert record here
                            Success=[db InsertValues:recored.Fields InTable:table_name];
                        }
                    }
                    else{
                        Success=[db InsertValues:recored.Fields InTable:table_name];
                    }
                    
                    

                }
                else{
                 Success=[db InsertValues:recored.Fields InTable:table_name];
                }
                
            }
    
    }
    @catch (NSException *exception) {
        Success=NO;
    }
    return Success;
}
-(NSMutableArray*)getDataFromCashWithSelectColumns:(id)selectColumns Tables:(NSArray*)table_list Where:(NSString*)where_condition FromIndex:(NSString*)fromIndex ToIndex:(NSString*)toIndex OrderByField:(NSString*)OrderField AscendingOrder:(BOOL)Order
{
    NSMutableArray* result=nil;
    @try {
        result =[db selectColumns:selectColumns fromTables:table_list where:where_condition andOrderBy:OrderField inAscendingOrder:Order withLimit:toIndex andOffect:fromIndex];
    }
    @catch (NSException *exception) {
        result=nil;
    }
    return  result;
}



@end
