//
//  UserDefaults.h
//  Islam Guide
//
//  Created by Hani on 7/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _H_UserDefaults
#define _H_UserDefaults

#import <Foundation/Foundation.h>


@interface UserDefaults : NSObject {
    
}

// Add Object
+ (void)addObject:(id)objectValue withKey:(NSString *)objectKey ifKeyNotExists:(BOOL)keyCheck;

// Get Object
+ (NSData *)getDataWithKey:(NSString *)dataKey;
+ (NSString *)getStringWithKey:(NSString *)stringKey;
+ (NSNumber *)getNumberWithKey:(NSString *)numberKey;
+ (NSDate *)getDateWithKey:(NSString *)dateKey;
+ (NSArray *)getArrayWithKey:(NSString *)arrayKey;
+ (NSDictionary *)getDictionaryWithKey:(NSString *)dictionaryKey;

// Check for Object
+ (BOOL)checkForKey:(NSString *)checkKey;

// Insert Object in existing object
+ (void)insertObject:(id)objectValue inArrayWithKey:(NSString *)arrayKey;
+ (void)insertObject:(id)objectValue withKey:(NSString *)objectKey inDictionaryWithKey:(NSString *)dictionaryKey;

@end

#endif