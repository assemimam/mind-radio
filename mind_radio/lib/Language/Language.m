//
//  Language.m
//  More
//
//  Created by Hani on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Language.h"
Language *LanguageObject = nil;

@interface Language ()
@property (nonatomic) availableLanguages currentLanguage;
@property (nonatomic) languageDirection currentLanguageDirection;
@property (nonatomic, retain) NSDictionary *imagesDictionary;
@property (nonatomic, retain) NSDictionary *stringsDictionary;

@end

@implementation Language

@synthesize currentLanguage, currentLanguageDirection, langAbbreviation;
@synthesize imagesDictionary, stringsDictionary;

#pragma mark -
#pragma mark Initialization

- (id)init{
	self = [super init];
	if (self) {
		if ([UserDefaults checkForKey:@"language"]) {
			[self setLanguage:[[UserDefaults getNumberWithKey:@"language"] intValue]];
		} else {
			[self setLanguage:languageEnglish];
		}
	}
	return self;
}

+ (Language *)getObject{
	if (LanguageObject == nil) {
		LanguageObject = [[Language alloc] init];
	}
	
	return LanguageObject;
}

#pragma mark -
#pragma mark Adjust Language

- (void)setLanguage:(availableLanguages)lang{
	[UserDefaults addObject:[NSNumber numberWithInt:lang] withKey:@"language" ifKeyNotExists:NO];
	self.currentLanguage = lang;
	[langAbbreviation release];
	
	switch (lang) {
		case languageArabic:
			langAbbreviation = @"ar";
			self.currentLanguageDirection = directionRTL;
			self.imagesDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ImagesArabic" ofType:@"plist"]];
			self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StringsArabic" ofType:@"plist"]];
			break;
		case languageEnglish:
			langAbbreviation = @"en";
			self.currentLanguageDirection = directionLTR;
			self.imagesDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ImagesEnglish" ofType:@"plist"]];
			self.stringsDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StringsEnglish" ofType:@"plist"]];
			break;
	}
}

- (availableLanguages)getLanguage{
	return self.currentLanguage;
}

- (languageDirection)getLanguageDirection{
	return self.currentLanguageDirection;
}

#pragma mark -
#pragma mark Get Data

- (NSString *)getStringWithKey:(NSString *)stringKey{
	return [self.stringsDictionary objectForKey:stringKey];
}

- (NSString *)getImageWithKey:(NSString *)imageKey{
	return [self.imagesDictionary objectForKey:imageKey];
}

- (NSArray *)getArrayWithKey:(NSString *)arrayKey
{
	return [self.stringsDictionary objectForKey:arrayKey];
}

#pragma mark -
#pragma mark Alert

- (void)alertNoInternet{
	//SHOW_ALERT([self getStringWithKey:@"no internet"], [self getStringWithKey:@"ok button"]);
}

- (void)showAlertWithKey:(NSString *)alertKey{
	//SHOW_ALERT([self getStringWithKey:alertKey], [self getStringWithKey:@"ok button"]);
}

- (void)alertNoSMS{
	//SHOW_ALERT([self getStringWithKey:@"check sms"], [self getStringWithKey:@"ok button"]);
}

- (void)alertNoMail{
	//SHOW_ALERT([self getStringWithKey:@"check mail"], [self getStringWithKey:@"ok button"]);
}

#pragma mark -
#pragma mark Memory Management

- (void)dealloc{
	[langAbbreviation release];
    [imagesDictionary release];
	[stringsDictionary release];
    [super dealloc];
}

@end
