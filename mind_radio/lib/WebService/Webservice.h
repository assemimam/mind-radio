//
//  Webservice.h
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PRODUCTION_VERSION

#ifdef PRODUCTION_VERSION
#define BASE_URL @"http://MindRadio.azurewebsites.net/Api/"
#else
#define BASE_URL @"http://MindRadio.azurewebsites.net/Api/"
#endif

@interface Webservice : NSObject
{
    
}
+(id)GetRadioUrl;
+(id)GetShows;
+(id)GetHosts;
+(id)GetAbout;
@end
