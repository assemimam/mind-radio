//
//  Webservice.m
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import "Webservice.h"

@implementation Webservice
+(id)GetAbout
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@about", BASE_URL]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
    

}
+(id)GetRadioUrl
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@radiourl", BASE_URL]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
    
 
}
+(id)GetShows
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@Shows", BASE_URL]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }

}
+(id)GetHosts
{
    @try {
        id result;
        @try {
            NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@Hosts", BASE_URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSMutableURLRequest *request = [NSURLRequest requestWithURL:url
                                                            cachePolicy:NSURLCacheStorageAllowed
                                                        timeoutInterval:120];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }
}
@end
