//
//  HostsViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface HostsViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *TryAgainView;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
    __weak IBOutlet UITableView *HostsTableView;
}
- (IBAction)TryAgainButtonAction:(UIButton *)sender;
@end
