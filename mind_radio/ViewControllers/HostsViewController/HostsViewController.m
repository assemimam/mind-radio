//
//  HostsViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "HostsViewController.h"
#import "HostCell.h"
#import "HostDetailsViewController.h"

@interface HostsViewController ()
{
    NSArray *hostsList;
}
@end

@implementation HostsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    hostsList = [[NSArray alloc]init];
    [LoadingView  setHidden:NO];
    [self.view bringSubviewToFront:LoadingView];
    LoadingLabel.font=[UIFont fontWithName:@"Arimo-Bold" size:LoadingLabel.font.pointSize];
    if (InternetConnection) {
        [NSThread detachNewThreadSelector:@selector(GetHostsList) toTarget:self withObject:nil];
    }
    else{
        [NSThread detachNewThreadSelector:@selector(GetCashedHosts) toTarget:self withObject:nil];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)HandleGetHostsListResult
{
    @try {
        if (hostsList) {
            [NSThread detachNewThreadSelector:@selector(CashHosts) toTarget:self withObject:nil];
        }
        else{
            
          [NSThread detachNewThreadSelector:@selector(GetCashedHosts) toTarget:self withObject:nil];
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleCashHostsResult:(id)result
{
    @try {
        [LoadingActivityIndicator stopAnimating];
        [self.view sendSubviewToBack:LoadingView];
        [LoadingView setHidden:YES];
        
        if (result) {
            if ([result count]>0) {
                hostsList = result;
            }
            else{
                if (!InternetConnection) {
                    [TryAgainView setHidden:NO];
                    [self.view bringSubviewToFront:TryAgainView];
                }
                else{
                  
                    [self.view bringSubviewToFront:TryAgainView];
                }

            }
            
        }
        else{
            [TryAgainView setHidden:NO];
            [self.view bringSubviewToFront:TryAgainView];
        }
        
        [HostsTableView reloadData];

    }
    @catch (NSException *exception) {
        
    }
}
-(void)GetCashedHosts
{
    @try {
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldHostId = [[DB_Field alloc]init];
        fldHostId.FieldName=@"HostId";
        
        DB_Field  *fldName= [[DB_Field alloc]init];
        fldName.FieldName=@"Name";
        
        DB_Field  *fldDetails= [[DB_Field alloc]init];
        fldDetails.FieldName=@"Details";
        
        DB_Field  *fldThumbnailURL= [[DB_Field alloc]init];
        fldThumbnailURL.FieldName=@"ThumbnailURL";
        
        DB_Field  *fldBio= [[DB_Field alloc]init];
        fldBio.FieldName=@"Bio";
        
        DB_Field  *fldHobbiesAndInterests= [[DB_Field alloc]init];
        fldHobbiesAndInterests.FieldName=@"HobbiesAndInterests";
        
        DB_Field  *fldWhatHeDoes= [[DB_Field alloc]init];
        fldWhatHeDoes.FieldName=@"WhatHeDoes";
        
        DB_Field  *fldContact= [[DB_Field alloc]init];
        fldContact.FieldName=@"Contact";
        
        id hostsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldHostId,fldName,fldDetails,fldThumbnailURL,fldBio,fldThumbnailURL,fldHobbiesAndInterests,fldWhatHeDoes,fldContact,nil] Tables:[NSArray arrayWithObject:@"hosts"] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        
       
        [self performSelectorOnMainThread:@selector(HandleCashHostsResult:) withObject:hostsResut waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashHosts
{
    @try {
        [[Cashing getObject] setDatabase:@"mind_radio"];
        for (id host in hostsList) {
            DB_Field  *fldHostId = [[DB_Field alloc]init];
            fldHostId.FieldName=@"HostId";
            fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
            fldHostId.IS_PRIMARY_KEY=YES;
            fldHostId.FieldValue=[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];
            
            DB_Field  *fldName= [[DB_Field alloc]init];
            fldName.FieldName=@"Name";
            fldName.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldName.FieldValue=[NSNull null]!=[host objectForKey:@"Name"]?[host objectForKey:@"Name"]:@"";
            
            DB_Field  *fldDetails= [[DB_Field alloc]init];
            fldDetails.FieldName=@"Details";
            fldDetails.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldDetails.FieldValue=[NSNull null]!=[host objectForKey:@"Details"]?[host objectForKey:@"Details"]:@"";
            
            DB_Field  *fldThumbnailURL= [[DB_Field alloc]init];
            fldThumbnailURL.FieldName=@"ThumbnailURL";
            fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
            fldThumbnailURL.FieldValue=[NSNull null]!=[host objectForKey:@"ThumbnailURL"]?[host objectForKey:@"ThumbnailURL"]:@"";
            
            DB_Field  *fldBio= [[DB_Field alloc]init];
            fldBio.FieldName=@"Bio";
            fldBio.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldBio.FieldValue=[NSNull null]!=[host objectForKey:@"Bio"]?[host objectForKey:@"Bio"]:@"";
            
            DB_Field  *fldHobbiesAndInterests= [[DB_Field alloc]init];
            fldHobbiesAndInterests.FieldName=@"HobbiesAndInterests";
            fldHobbiesAndInterests.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldHobbiesAndInterests.FieldValue=[NSNull null]!=[host objectForKey:@"HobbiesAndInterests"]?[host objectForKey:@"HobbiesAndInterests"]:@"";
            
            DB_Field  *fldWhatHeDoes= [[DB_Field alloc]init];
            fldWhatHeDoes.FieldName=@"WhatHeDoes";
            fldWhatHeDoes.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldWhatHeDoes.FieldValue=[NSNull null]!=[host objectForKey:@"WhatHeDoes"]?[host objectForKey:@"WhatHeDoes"]:@"";
            
            DB_Field  *fldContact= [[DB_Field alloc]init];
            fldContact.FieldName=@"Contact";
            fldContact.FieldDataType=FIELD_DATA_TYPE_TEXT;
            fldContact.FieldValue=[NSNull null]!=[host objectForKey:@"Contact"]?[host objectForKey:@"Contact"]:@"";

            
            DB_Recored *hostAddedRecored = [[DB_Recored alloc]init];
            hostAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostId,fldName,fldDetails,fldThumbnailURL,fldBio,fldHobbiesAndInterests,fldWhatHeDoes,fldContact, nil];
            
            BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:hostAddedRecored] inTable:@"hosts"];
        
            
        }
        
        [self performSelectorOnMainThread:@selector(HandleCashHostsResult:) withObject:hostsList waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetHostsList
{
    @try {
        hostsList = [Webservice GetHosts];
        [self performSelectorOnMainThread:@selector(HandleGetHostsListResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HostCell *cell;
    @try {
        
        cell = (HostCell*)[tableView dequeueReusableCellWithIdentifier:@"cleHost"];
        if (cell == nil) {
            cell =(HostCell*)[[[NSBundle mainBundle]loadNibNamed:@"HostCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSString *imageUrlString =[[[hostsList objectAtIndex:indexPath.row]objectForKey:@"ThumbnailURL"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
        if (imageUrlString.length >4) {
            if ([[imageUrlString substringToIndex:4] isEqualToString:@"http"]) {
                NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
                if (imageUrl) {
                    [cell.LoadingActivityIndicator startAnimating];
                    [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                        if (image) {
                            cell.HostImageView.image = image;
                        }
                        else{
                            cell.HostImageView.image = [UIImage imageNamed:@"defaulthosts-ic.png"];
                        }
                        [cell.LoadingActivityIndicator stopAnimating];
                    }];
                }
                else{
                    
                }
            }
            else{
                NSString *path =[[hostsList objectAtIndex:indexPath.row]objectForKey:@"ThumbnailURL"];
                NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
                
                NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
                long long fileSize = [fileSizeNumber longLongValue]/1024;
                if (fileSize<500) {
                    NSData *imageData =[NSData dataWithContentsOfFile:path];
                    if (imageData) {
                        cell.HostImageView.image  = [UIImage imageWithData:imageData];
                        imageData=nil;
                    }
                    else{
                        cell.HostImageView.image  = [UIImage imageNamed:@"defaulthosts-ic.png"];
                    }
                    
                }
                else
                {
                    cell.HostImageView.image  = [UIImage imageNamed:@"defaulthosts-ic.png"];
                }
                
                [cell.LoadingActivityIndicator stopAnimating];
            }
  
            
        }
        
    
                
        
        cell.BackgroundButton.tag = indexPath.row;
        [cell.BackgroundButton addTarget:self action:@selector(BackgroundButtonAction:)  forControlEvents:UIControlEventTouchUpInside];
        cell.HostNameLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:cell.HostNameLabel.font.pointSize];
        cell.HostNameLabel.text =[[hostsList objectAtIndex:indexPath.row]objectForKey:@"Name"];
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [hostsList count];
}
#pragma -mark uitableview delegate methods

-(void)BackgroundButtonAction:(UIButton*)sender
{
    @try {
        HostDetailsViewController *vc_hostDetails = [[HostDetailsViewController alloc]init];
        vc_hostDetails.HostDetails = [hostsList objectAtIndex:sender.tag];
        [self.navigationController pushViewController:vc_hostDetails animated:YES];
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)TryAgainButtonAction:(UIButton *)sender {
    @try {
        [LoadingView setHidden:NO];
        [LoadingActivityIndicator startAnimating];
        [self.view bringSubviewToFront:LoadingView];
        
        [TryAgainView setHidden:YES];
        [self.view sendSubviewToBack:TryAgainView];
        
        hostsList = [[NSArray alloc]init];
        if (InternetConnection) {
            [NSThread detachNewThreadSelector:@selector(GetHostsList) toTarget:self withObject:nil];
        }
        else{
            [NSThread detachNewThreadSelector:@selector(GetCashedHosts) toTarget:self withObject:nil];
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
