//
//  ViewController.m
//  mind_radio
//
//  Created by Assem Imam on 11/18/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "SplashScreenViewController.h"
#import  "StreamingViewController.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)GotoStreamingView
{
    @try {
        StreamingViewController *vc_streamin = [[StreamingViewController alloc]init];
        [self.navigationController pushViewController:vc_streamin animated:NO];
        
    }
    @catch (NSException *exception) {
      
    }

}
- (void)viewDidLoad
{
      self.navigationController.navigationBarHidden=YES;
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    [super viewDidLoad];
    [self performSelector:@selector(GotoStreamingView) withObject:nil afterDelay:0.3];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
