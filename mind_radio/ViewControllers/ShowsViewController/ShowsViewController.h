//
//  ShowsViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MasterViewController.h"

@interface ShowsViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *TryAgainView;
    __weak IBOutlet UIView *NoDataView;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
    __weak IBOutlet UITableView *ShowsTableView;
}
- (IBAction)TryAgainButtonAction:(UIButton *)sender;
@end
