//
//  ShowsViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "ShowsViewController.h"
#import "ShowCell.h"
#import "ShowDetailsViewController.h"

@interface ShowsViewController ()
{
    NSMutableArray *showsList;
    BOOL fromCashedData;
}
@end

@implementation ShowsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    fromCashedData = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    showsList = [[NSMutableArray alloc]init];
    [LoadingView setHidden:NO];
    [self.view bringSubviewToFront:LoadingView];
    LoadingLabel.font=[UIFont fontWithName:@"Arimo-Bold" size:LoadingLabel.font.pointSize];
    if (InternetConnection) {
        [NSThread detachNewThreadSelector:@selector(GetShowsList) toTarget:self withObject:nil];
    }
    else{
        id result=  [self GetCashedShows];
        [self HandleCashShowsResult:result];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSMutableArray*)GetExistanceIndexesOfTheShow:(NSString*)show_name InShowList:(NSArray*)showList ForKey:(NSString*)key
{
    NSMutableArray* returnIndexes=[[NSMutableArray alloc]init];
    @try {
        int objectIndex = 0;
        int index=0;
        
        for (id show in showList) {
            if ([[[show objectForKey:key] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] caseInsensitiveCompare:[show_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]==NSOrderedSame)
            {
                if (index!=0) {
                    [returnIndexes addObject:[NSString stringWithFormat:@"%i",objectIndex]];
                    break;
                }
                index++;
            }
            objectIndex ++;
        }
    }
    @catch (NSException *exception) {
        
    }
    return returnIndexes;
}
-(id)GetCashedShows
{
    fromCashedData =YES;
    NSMutableDictionary *ShowDisctionary = [[NSMutableDictionary alloc]init];
    @try {
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";
        
        DB_Field  *fldDayName = [[DB_Field alloc]init];
        fldDayName.FieldName=@"DayName";
        
        DB_Field  *fldDayOfWeek= [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
        
        DB_Field  *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
        fldStartTimeStr.FieldName=@"StartTimeStr";
        
        DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
        fldEndTimeStr.FieldName=@"EndTimeStr";
        
        DB_Field  *fldTimeZone = [[DB_Field alloc]init];
        fldTimeZone.FieldName=@"TimeZone";
        fldTimeZone.FieldAlias=@"TimeZoneOffset";
        
        DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
        fldThumbNailURL.FieldName=@"ThumbNailURL";
        
        DB_Field  *fldDescriptione = [[DB_Field alloc]init];
        fldDescriptione.FieldName=@"Description";
        
        id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldDayName,fldDayOfWeek,fldDescriptione,fldStartTimeStr,fldEndTimeStr,fldThumbNailURL,fldTitle,fldTimeZone,nil] Tables:[NSArray arrayWithObject:@"shows"] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        NSMutableArray *showsArray = [[NSMutableArray alloc]init];
        
        
        if (showsResut) {
            if ([showsResut count]> 0) {
                [ShowDisctionary setObject:[[showsResut objectAtIndex:0]objectForKey:@"TimeZoneOffset"] forKey:@"TimeZoneOffset"];
                for (id show in showsResut) {
                    NSMutableDictionary *showsDic = [show mutableCopy];
                    DB_Field  *fldHostId = [[DB_Field alloc]init];
                    fldHostId.FieldName=@"id";
                    
                    DB_Field  *fldHostName = [[DB_Field alloc]init];
                    fldHostName.FieldName=@"Name";
                    
                    DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                    fldThumbnailURL.FieldName=@"ThumbnailURL";
                    
                    id hostsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldHostId,fldHostName,fldThumbnailURL,nil] Tables:[NSArray arrayWithObject:@"show_hosts"] Where:[NSString stringWithFormat:@"ShowId=%@",[show objectForKey:@"ShowId"]] FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
                    if (hostsResut) {
                        NSMutableArray *hostsArray = [[NSMutableArray alloc]init];
                        for (id host in hostsResut) {
                            
                            if ([host objectForKey:@"Name"] && ![[host objectForKey:@"Name"] isEqualToString:@"null"]&& [NSNull null]!= [host objectForKey:@"Name"]) {
                                [hostsArray addObject:host];
                            }
                        }
                        
                        [showsDic setObject:hostsArray forKey:@"Hosts"];
                    }
                    
                    [showsArray addObject:showsDic];
                }
                
                NSPredicate *saturdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Saturday'"];
                id saturdayShowsList = [showsArray filteredArrayUsingPredicate:saturdayPridicate];
                [ShowDisctionary setObject:saturdayShowsList forKey:@"Saturday"];
                
                NSPredicate *sundayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Sunday'"];
                id sundayShowsList = [showsArray filteredArrayUsingPredicate:sundayPridicate];
                [ShowDisctionary setObject:sundayShowsList forKey:@"Sunday"];
                
                NSPredicate *mondayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Monday'"];
                id mondayShowsList = [showsArray filteredArrayUsingPredicate:mondayPridicate];
                [ShowDisctionary setObject:mondayShowsList forKey:@"Monday"];
                
                NSPredicate *tusedayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Tuseday'"];
                id tusedayShowsList = [showsArray filteredArrayUsingPredicate:tusedayPridicate];
                [ShowDisctionary setObject:tusedayShowsList forKey:@"Tuseday"];
                
                NSPredicate *wednesdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Wednesday'"];
                id wednesdayShowsList = [showsArray filteredArrayUsingPredicate:wednesdayPridicate];
                [ShowDisctionary setObject:wednesdayShowsList forKey:@"Wednesday"];
                
                NSPredicate *thursdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Thursday'"];
                id thursdayShowsList = [showsArray filteredArrayUsingPredicate:thursdayPridicate];
                [ShowDisctionary setObject:thursdayShowsList forKey:@"Thursday"];
                
                NSPredicate *fridayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Friday'"];
                id fridayShowsList = [showsArray filteredArrayUsingPredicate:fridayPridicate];
                [ShowDisctionary setObject:fridayShowsList forKey:@"Friday"];
                
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    return ShowDisctionary;
}
-(void)GetShowsList
{
    @try {
        fromCashedData =NO;
        id result = [Webservice GetShows];
        [self performSelectorOnMainThread:@selector(HandleGetShows:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)CashShows:(id)result
{
    @try {
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        if ([NSNull null]!= [result objectForKey:@"Saturday"]) {
            for (id show in [result objectForKey:@"Saturday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Saturday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Sunday"]) {
            for (id show in [result objectForKey:@"Sunday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Sunday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Monday"]) {
            for (id show in [result objectForKey:@"Monday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Monday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Tuseday"]) {
            for (id show in [result objectForKey:@"Tuseday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Tuseday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Wednesday"]) {
            for (id show in [result objectForKey:@"Wednesday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Wednesday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Thursday"]) {
            for (id show in [result objectForKey:@"Thursday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Thursday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Friday"]) {
            for (id show in [result objectForKey:@"Friday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Friday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        [self performSelectorOnMainThread:@selector(HandleCashShowsResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)HandleCashShowsResult:(id)result
{
    @try {
        
        if ([result count]>0) {
            NSMutableArray *showResult = [[NSMutableArray alloc]init];
            for (id show in [result objectForKey:@"Saturday"]  ) {
                
                [showResult  addObject:show];
                
            }
            for (id show in [result objectForKey:@"Sunday"]  ) {
                
                [showResult  addObject:show];
                
            }
            for (id show in [result objectForKey:@"Monday"]  ) {
                
                [showResult  addObject:show];
            }
            
            for (id show in [result objectForKey:@"Tuseday"]  ) {
                [showResult  addObject:show];
            }
            
            for (id show in [result objectForKey:@"Wednesday"]  ) {
                [showResult  addObject:show];
            }
            for (id show in [result objectForKey:@"Thursday"]  ) {
                
                [showResult  addObject:show];
            }
            for (id show in [result objectForKey:@"Friday"]  ) {
                
                [showResult  addObject:show];
                
            }
            
            NSMutableArray*accumlatedDublicatedIndexes = [[NSMutableArray alloc]init];
            for (id show in showResult) {
                NSMutableArray*dublicatedIndexes = [self GetExistanceIndexesOfTheShow:[show objectForKey:@"Title"] InShowList:showResult ForKey:@"Title"];
                if ([dublicatedIndexes count]==0) {
                    [showsList addObject:show];
                }
                else{
                    for (id index in dublicatedIndexes) {
                        [accumlatedDublicatedIndexes addObject:index];
                    }
                }
            }
            if ([accumlatedDublicatedIndexes count]>0) {
                NSArray *states = accumlatedDublicatedIndexes;
                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:states];
                NSSet *uniqueStates = [orderedSet set];
                for (id index in uniqueStates) {
                    [showsList addObject:[showResult objectAtIndex:[index intValue]]];
                }
            }
        }
        else{
            if (!InternetConnection) {
                [ShowsTableView setHidden:YES];
                [TryAgainView setHidden:NO];
                [self.view bringSubviewToFront:TryAgainView];
            }
            else{
                [ShowsTableView setHidden:YES];
                [NoDataView  setHidden:NO];
                [self.view bringSubviewToFront:NoDataView];
            }
            
        }
        
        NSSortDescriptor * titleDescriptor = [[NSSortDescriptor alloc]
                                              initWithKey:@"Title" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortDescriptors = @[titleDescriptor];
        showsList = [[showsList sortedArrayUsingDescriptors:sortDescriptors]mutableCopy];
        
        [ShowsTableView reloadData];
        [LoadingActivityIndicator stopAnimating];
        [self.view sendSubviewToBack:LoadingView];
        [LoadingView setHidden:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)HandleGetShows:(id)result
{
    @try {
        if (result) {
            [NSThread detachNewThreadSelector:@selector(CashShows:) toTarget:self withObject:result];
        }
        else{
            id result=  [self GetCashedShows];
            [self HandleCashShowsResult:result];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShowCell *cell;
    @try {
        //cell = (HostCell*)[tableView dequeueReusableCellWithIdentifier:@"cleHost"];
        if (cell == nil) {
            cell =(ShowCell*)[[[NSBundle mainBundle]loadNibNamed:@"ShowCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSString *imageUrlString = [NSNull null]!=[[showsList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"]?[[showsList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"]:@"";
        
        NSURL *imageUrl = [NSURL URLWithString:[imageUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        if (fromCashedData) {
            [cell.LoadingActivityIndicator startAnimating];
            NSString *path =[[showsList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"];
            
            NSData *imageData =[NSData dataWithContentsOfFile:path];
            if (imageData) {
                cell.ShowImageView.image = [UIImage imageWithData:imageData];
            }
            else{
                cell.ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
            }
            [cell.LoadingActivityIndicator stopAnimating];
        }
        else{
            if (imageUrl) {
                [cell.LoadingActivityIndicator startAnimating];
                [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                    if (image) {
                        cell.ShowImageView.image = image;
                    }
                    else{
                        cell.ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
                    }
                    [cell.LoadingActivityIndicator stopAnimating];
                }];
            }
        }
        
        cell.ShowNameLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:cell.ShowNameLabel.font.pointSize];
        cell.HostsLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:cell.HostsLabel.font.pointSize];
        cell.ShowNameLabel.text =[NSNull null]!=[[showsList objectAtIndex:indexPath.row]objectForKey:@"Title"]?[[showsList objectAtIndex:indexPath.row]objectForKey:@"Title"]:@"";
        
        NSString *hostsString=@"";
        if ([NSNull null]!=[[showsList objectAtIndex:indexPath.row]objectForKey:@"Hosts"] ) {
            for (id host in  [[showsList objectAtIndex:indexPath.row]objectForKey:@"Hosts"]) {
                if ([host objectForKey:@"Name"] && [NSNull null]!=[host objectForKey:@"Name"] && ![[host objectForKey:@"Name"] isEqualToString:@"(null)"]) {
                    if ([[host objectForKey:@"Name"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
                        hostsString =[hostsString stringByAppendingFormat:@"%@,",[host objectForKey:@"Name"]];
                    }
                }
            }
            hostsString = [hostsString substringToIndex:[hostsString length]-1];
            cell.HostsLabel.text = hostsString;
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [showsList count];
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        ShowDetailsViewController *vc_showDetails = [[ShowDetailsViewController alloc]init];
        vc_showDetails.ShowDetails = [showsList objectAtIndex:indexPath.row];
        vc_showDetails.comeFromCash = fromCashedData;
        [self.navigationController pushViewController:vc_showDetails animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}


- (IBAction)TryAgainButtonAction:(UIButton *)sender {
    
    @try {
        [TryAgainView setHidden:YES];
        [self.view sendSubviewToBack:TryAgainView];
        [LoadingView setHidden:NO];
        [LoadingActivityIndicator startAnimating];
        [self.view bringSubviewToFront:LoadingView];
        if (InternetConnection) {
            [NSThread detachNewThreadSelector:@selector(GetShowsList) toTarget:self withObject:nil];
        }
        else{
            id result=  [self GetCashedShows];
            [self HandleCashShowsResult:result];
            if (result) {
                if ([result count]==0 ) {
                    [LoadingActivityIndicator stopAnimating];
                    [self.view sendSubviewToBack:LoadingView];
                    [LoadingView setHidden:YES];
                    
                    [TryAgainView setHidden:NO];
                    [self.view bringSubviewToFront:TryAgainView];
                }
            }
            else{
                [LoadingActivityIndicator stopAnimating];
                [self.view sendSubviewToBack:LoadingView];
                [LoadingView setHidden:YES];
                
                [TryAgainView setHidden:NO];
                [self.view bringSubviewToFront:TryAgainView];
            }
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
