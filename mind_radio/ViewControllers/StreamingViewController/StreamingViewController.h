//
//  StreamingViewController.h
//  SAUDI_KAU
//
//  Created by assem on 7/21/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AudioStreamer;
#import "AudioButton.h"
#import  "MasterViewController.h"
#import "LibrariesHeader.h"
#import "MarqueeLabel.h"

@interface StreamingViewController : MasterViewController
{
    __weak IBOutlet MarqueeLabel *NowPlayingLabel;
    __weak IBOutlet UIImageView *ShowImageView;
    __weak IBOutlet UIImageView *PlayImageView;
    __weak IBOutlet UIView *VolumeView;
    __weak IBOutlet MarqueeLabel *NowPlayingProgramNameLabel;
    __weak IBOutlet MarqueeLabel *NowPlayingTitleLabel;
    float LeftChannel;
    float  RightChannel;
    __weak IBOutlet UIButton *StreamingButton;
    __weak IBOutlet UIView *LeftEquilizerView;
    __weak IBOutlet UIView *RightEquilizerView;
    __weak IBOutlet UIView *SeconedLeftEquilizerView;
    __weak IBOutlet UIView *SeconedRightEquilizerView;
     NSTimer *progressUpdateTimer;
     NSTimer *currentShowTimer;
}
- (IBAction)ShareAction:(UIButton *)sender;
- (IBAction)PlayButtonAction:(UIButton *)sender;
- (IBAction)MuteAction:(UIButton *)sender;
- (IBAction)FullVolumAction:(UIButton *)sender;
@end
