//
//  StreamingViewController.m
//  SAUDI_KAU
//
//  Created by assem on 7/21/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "StreamingViewController.h"
#import "AudioStreamer.h"
#import "SchedualViewController.h"
#import <Social/Social.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import "DCKnob.h"
#import <CFNetwork/CFNetwork.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaItem.h>
#import <AVFoundation/AVFoundation.h>
MPMediaItemArtwork *albumArt;

@interface StreamingViewController () <DCControlDelegate,AudioStreamerDelegate>
{
    AudioStreamer *streamer;
    BOOL IS_PLAY;
    NSString *audioUrl;
    MPVolumeView *volumeController;
    float soundLevel;
    NSString *streamArtist;
    NSString *streamTitle;
    NSString *streamAlbum;
    NSString *currentShowName;
}
@property (nonatomic, retain) AudioStreamer *streamer;
@property (nonatomic, strong) CAShapeLayer *LeftEquilizerShapeLayer;
@property (nonatomic, strong) CAShapeLayer *RightEquilizerShapeLayer;
@property (nonatomic, strong) CAShapeLayer *SeconedLeftEquilizerShapeLayer;
@property (nonatomic, strong) CAShapeLayer *SeconedRightEquilizerShapeLayer;
@property (nonatomic, retain) DCKnob *VolumeSliderControl;
@end

@implementation StreamingViewController
@synthesize streamer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma -mark CurrentShow Method
-(void)GetCurrentPlayedShow
{
    @try {
     
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
         dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        NSString *currentDayName = [dateFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
        [timeFormatter setDateFormat:@"HH:mm:ss"];
        timeFormatter.timeZone=[NSTimeZone localTimeZone];
        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
      
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";
        
        DB_Field  *fldDayName = [[DB_Field alloc]init];
        fldDayName.FieldName=@"DayName";
        
        DB_Field  *fldDayOfWeek= [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
        
        DB_Field  *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
        fldStartTimeStr.FieldName=@"time(strftime('%H:%M:%S',StartTimeStr))";
        fldStartTimeStr.FieldAlias = @"StartTimeStr";
        
        DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
        fldEndTimeStr.FieldName=@"EndTimeStr";
        
        DB_Field  *fldTimeZone = [[DB_Field alloc]init];
        fldTimeZone.FieldName=@"TimeZone";
        fldTimeZone.FieldAlias=@"TimeZoneOffset";
        
        DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
        fldThumbNailURL.FieldName=@"ThumbNailURL";
        
        DB_Field  *fldDescriptione = [[DB_Field alloc]init];
        fldDescriptione.FieldName=@"Description";
        id showTimeZoneRsult = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldTimeZone,nil] Tables:[NSArray arrayWithObject:@"shows"] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        float showTimeZone;
        if ([showTimeZoneRsult count]>0) {
            showTimeZone = [[[showTimeZoneRsult objectAtIndex:0]objectForKey:@"TimeZoneOffset"]floatValue];
            NSString *currentTime =[timeFormatter stringFromDate:[NSDate date]];
            NSString *convertedCurrentTime = [self ConvertToDeviceTime:currentTime AndTimeOffset:showTimeZone];
            NSString *whereCondition =[NSString stringWithFormat:@"(DayOfWeek='%@' and( (time('%@')>= StartTimeStr and time('%@')<=EndTimeStr))) ",currentDayName,convertedCurrentTime,convertedCurrentTime];
         
            id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldDayName,fldDayOfWeek,fldDescriptione,fldStartTimeStr,fldEndTimeStr,fldThumbNailURL,fldTitle,fldTimeZone,nil] Tables:[NSArray arrayWithObject:@"shows"] Where:whereCondition FromIndex:nil ToIndex:nil OrderByField:@"StartTimeStr" AscendingOrder:YES];
            
            if (showsResut) {
                if ([showsResut count]>0) {
                    currentShowName=[[showsResut objectAtIndex:0]objectForKey:@"Title"];
                    if ([currentDayName stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
                        currentShowName=@"Mind Radio";
                    }
                    NSString *path =[[showsResut objectAtIndex:0]objectForKey:@"ThumbNailURL"];
                    NSData *imageData =[NSData dataWithContentsOfFile:path];
                    if (imageData) {
                        ShowImageView.image = [UIImage imageWithData:imageData];
                    }
                    else{
                        ShowImageView.image =[UIImage imageNamed:@"circle-defult.png"];
                    }
                }
                else{
                    currentShowName=@"Mind Radio";
                    ShowImageView.image =[UIImage imageNamed:@"circle-defult.png"];
                }
            }
            else{
                currentShowName=@"Mind Radio";
                ShowImageView.image =[UIImage imageNamed:@"circle-defult.png"];
            }
            if (IS_PLAY) {
                NowPlayingTitleLabel.text=currentShowName;
            }

            
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark AudioStreamer Delegate
-(void)AudioFailWithError:(AudioStreamerErrorCode)error
{
    @try {
        switch (error)
        {
            case AS_NO_ERROR:
                break;
            case AS_FILE_STREAM_GET_PROPERTY_FAILED:
                break;
            case AS_FILE_STREAM_SEEK_FAILED:
                break;
            case AS_FILE_STREAM_PARSE_BYTES_FAILED:
                break;
            case AS_AUDIO_QUEUE_CREATION_FAILED:
                break;
            case AS_AUDIO_QUEUE_BUFFER_ALLOCATION_FAILED:
                break;
            case AS_AUDIO_QUEUE_ENQUEUE_FAILED:
                break;
            case AS_AUDIO_QUEUE_ADD_LISTENER_FAILED:
                break;
            case AS_AUDIO_QUEUE_REMOVE_LISTENER_FAILED:
                break;
            case AS_AUDIO_QUEUE_START_FAILED:
                break;
            case AS_AUDIO_QUEUE_BUFFER_MISMATCH:
                break;
            case AS_FILE_STREAM_OPEN_FAILED:
                break;
            case AS_FILE_STREAM_CLOSE_FAILED:
                break;
            case AS_AUDIO_QUEUE_DISPOSE_FAILED:
                break;
            case AS_AUDIO_QUEUE_PAUSE_FAILED:
                break;
            case AS_AUDIO_QUEUE_FLUSH_FAILED:
                break;
            case AS_AUDIO_DATA_NOT_FOUND:
            {
               
                [StreamingButton setImage:[UIImage imageNamed:@"weareoffic.png"] forState:UIControlStateNormal];
                [StreamingButton setImage:[UIImage imageNamed:@"weareoffic.png"] forState:UIControlStateHighlighted];
                NowPlayingLabel.textColor = [UIColor blackColor];
                NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
                NowPlayingLabel.text =@"WE ARE OFF TODAY";
            }
                break;
            case AS_GET_AUDIO_TIME_FAILED:
                break;
            case AS_NETWORK_CONNECTION_FAILED:
            {
                NowPlayingLabel.textColor = [UIColor blackColor];
                NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
                NowPlayingLabel.text =@"FAILED TO CONNECT";
                NowPlayingTitleLabel.text =@"";
                NowPlayingProgramNameLabel.text=@"";
                [ShowImageView setHidden:YES];
            }
                break;
            case AS_AUDIO_QUEUE_STOP_FAILED:
                break;
            case AS_AUDIO_STREAMER_FAILED:
                break;
            case AS_AUDIO_BUFFER_TOO_SMALL:
                
                break;
                
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma mark DCControlDelegate
- (void)controlValueDidChange:(float)value sender:(id)sender
{
    @try {
        NSArray *windows = [UIApplication sharedApplication].windows;
        volumeController.alpha = 0.1f;
        UISlider *volumeViewSlider;
        
        for (UIView *view in [volumeController subviews]){
            if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
                volumeViewSlider = (UISlider *) view;
            }
        }
        [volumeViewSlider  setValue:value/100];
        
        if (windows.count > 0) {
            [[windows objectAtIndex:0] addSubview:volumeController];
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark Metadata methods
- (void)MetadataChanged:(NSNotification *)aNotification
{
    @try {
        
        //NSLog(@"Raw meta data = %@", [[aNotification userInfo] objectForKey:@"metadata"]);
        
        NSArray *metaParts = [[[aNotification userInfo] objectForKey:@"metadata"] componentsSeparatedByString:@";"];
        NSString *item;
        NSMutableDictionary *hash = [[NSMutableDictionary alloc] init];
        for (item in metaParts) {
            // split the key/value pair
            NSArray *pair = [item componentsSeparatedByString:@"="];
            // don't bother with bad metadata
            if ([pair count] == 2)
                [hash setObject:[pair objectAtIndex:1] forKey:[pair objectAtIndex:0]];
        }
        
        // do something with the StreamTitle
        NSString *streamString = [[hash objectForKey:@"StreamTitle"] stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        NSArray *streamParts = [streamString componentsSeparatedByString:@" - "];
        if ([streamParts count] > 0) {
            streamArtist = [streamParts objectAtIndex:0];
        } else {
            streamArtist = @"";
        }
        // this looks odd but not every server will have all artist hyphen title
        if ([streamParts count] >= 2) {
            streamTitle = [streamParts objectAtIndex:1];
            if ([streamParts count] >= 3) {
                streamAlbum = [streamParts objectAtIndex:2];
            } else {
                streamAlbum = @"N/A";
            }
        } else {
            streamTitle = @"";
            streamAlbum = @"";
        }
      	NSLog(@"%@ by %@ from %@", streamTitle, streamArtist, streamAlbum);
        
        //NowPlayingTitleLabel.text= streamArtist;
        NowPlayingProgramNameLabel.text = streamTitle;
        NowPlayingProgramNameLabel.marqueeType = MLContinuousReverse;
        NowPlayingProgramNameLabel.scrollDuration = 8.0;
        NowPlayingProgramNameLabel.fadeLength = 15.0f;
        
//        Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
//        
//        if (playingInfoCenter)
//        {
//            albumArt = [[MPMediaItemArtwork alloc] initWithImage: [UIImage imageNamed:@"iTunesArtwork"]];
//            NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
//            [songInfo setObject:streamTitle forKey:MPMediaItemPropertyTitle];
//            [songInfo setObject:NowPlayingTitleLabel forKey:MPMediaItemPropertyAlbumTitle];
//            [songInfo setObject:streamArtist forKey:MPMediaItemPropertyArtist];
//            [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
//            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
//        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark Streamer PlayBack methods
- (void)PlaybackStateChanged:(NSNotification *)aNotification
{
    @try {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        if ([streamer isWaiting])
        {
            [ShowImageView setHidden:NO];
            [NowPlayingTitleLabel setHidden:NO];
            
            IS_PLAY =NO;
            [streamer setMeteringEnabled:NO];
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
            NowPlayingLabel.textColor = [UIColor blackColor];
            NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
            NowPlayingLabel.text=@"LOADING...";
            NSLog(@"is waiting");
        }
       
        else if ([streamer isPlaying])
        {
            [ShowImageView setHidden:NO];
            [NowPlayingTitleLabel setHidden:NO];
            IS_PLAY =YES;

            [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
            [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
            [streamer setMeteringEnabled:YES];
            NowPlayingLabel.textColor = [UIColor colorWithRed:166.0f/255.0f green:24.0f/255.0f blue:32.0f/255.0f alpha:1];
            NowPlayingLabel.font = [UIFont systemFontOfSize: NowPlayingLabel.font.pointSize];
            NowPlayingLabel.text=@"NOW PLAYING";
            NSLog(@"is playing");
        }
        else if ([streamer isPaused]) {
            [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
            [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
            [ShowImageView setHidden:YES];
            [NowPlayingTitleLabel setHidden:YES];
            IS_PLAY =NO;
            [streamer setMeteringEnabled:NO];
            NSLog(@"is paused");
        }
        else if ([streamer isIdle])
        {
            
            if (streamer) {
                [ShowImageView setHidden:YES];
                [NowPlayingTitleLabel setHidden:YES];
                [NowPlayingProgramNameLabel setHidden:YES];
                [self destroyStreamer];
                IS_PLAY =NO;
                [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                NowPlayingLabel.textColor = [UIColor blackColor];
                NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
                NowPlayingLabel.text=@"FAILED TO CONNECT";
                [streamer setMeteringEnabled:NO];
            }
            
            NSLog(@"is idle");
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark Streamer LifyCycle methods
- (void)initializeNewStreamer
{
    @try {
        [self destroyStreamer];
        
        [streamer stop];
        streamer = nil;
        streamer = [[AudioStreamer alloc] initWithURL:[NSURL URLWithString:audioUrl]];
        [streamer setMeteringEnabled:YES];
        // set up display updater
        
        // register the streamer on notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(PlaybackStateChanged:)
                                                     name:ASStatusChangedNotification
                                                   object:streamer];
        
    }
    @catch (NSException *exception) {
        
    }
}


- (void)destroyStreamer
{
    @try {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASStatusChangedNotification
         object:streamer];

        [streamer stop];
        streamer = nil;
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HandleGetAudioPlayerURL
{
    @try {
        
        if (audioUrl == nil) {
            NowPlayingLabel.textColor = [UIColor blackColor];
            NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
            NowPlayingLabel.text =@"WE ARE OFF TODAY";
            NowPlayingTitleLabel.text =@"";
            return;
        }
        IS_PLAY =NO;

        audioUrl =
        [audioUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""]
        ;
        if ([NSURL URLWithString:audioUrl]) {
           
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            if (streamer) {
                [self destroyStreamer];
            }
             if (!streamer) {
                 [self initializeNewStreamer];
                 
                 [streamer start];
             }
           
            [ShowImageView setHidden:NO];
            NowPlayingProgramNameLabel.text =streamTitle;
            NowPlayingTitleLabel.text =currentShowName;
            [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
            [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
            
            [[NSNotificationCenter defaultCenter]
             addObserver:self
             selector:@selector(MetadataChanged:)
             name:ASUpdateMetadataNotification
             object:streamer];
            
            if (progressUpdateTimer)
            {
                [progressUpdateTimer invalidate];
                progressUpdateTimer = nil;
            }
            progressUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                   target:self
                                                                 selector:@selector(UpdateEquilizerLevels:)
                                                                 userInfo:nil
                                                                repeats:YES];
            if (currentShowTimer)
            {
                [currentShowTimer invalidate];
                currentShowTimer = nil;
            }
            currentShowTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                   target:self
                                                                 selector:@selector(GetCurrentPlayedShow)
                                                                 userInfo:nil
                                                                  repeats:YES];
        }
        else{
            NowPlayingLabel.textColor = [UIColor blackColor];
            NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
            NowPlayingLabel.text=@"WE ARE OFF TODAY ";
            NowPlayingProgramNameLabel.text=@"";
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)GetAudioPlayerURL
{
    @try {

        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"audio_url"]) {
           audioUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"audio_url"];
        }
        else{
            id  url = [Webservice GetRadioUrl];
            if ([NSNull null]!= url && ![url isEqualToString:@"null"]&& url) {
                audioUrl = url;
                [[NSUserDefaults standardUserDefaults]setObject:audioUrl forKey:@"audio_url"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else{
                if ([[NSUserDefaults standardUserDefaults] objectForKey:@"audio_url"]) {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"audio_url"] length]>3) {
                        audioUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"audio_url"];
                    }
                    else{
                        audioUrl=nil;
                    }
                }
                else{
                    audioUrl=nil;
                }
            }

        }
#pragma -mark default url for test incase channel is off comment it when publish the application
       // audioUrl =@"http://199.180.72.2:8015";
        audioUrl =@"http://www.s7.voscast.com:7814/";
        [self performSelectorOnMainThread:@selector(HandleGetAudioPlayerURL) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)viewDidLoad
{
    @try {
        
        [super viewDidLoad];
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
        self.navigationController.navigationBarHidden=YES;
        self.navigationController.viewControllers = [NSArray arrayWithObject:self];
        
        NowPlayingProgramNameLabel.text=@"";
        [ShowImageView setHidden:YES];
        streamer.delegate = self;
        IS_PLAY=NO;
        ////////////////////////////////////////////////
        Reachability *internetReach = [Reachability reachabilityForInternetConnection];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(internetReachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        [internetReach startNotifier];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(volumeChanged:)
         name:@"AVSystemController_SystemVolumeDidChangeNotification"
         object:nil];
        
        volumeController= [[MPVolumeView alloc] initWithFrame:CGRectMake(-2000., -2000., 0.f, 0.f)];
        
        NSArray *windows = [UIApplication sharedApplication].windows;
        volumeController.alpha = 0.1f;
        UISlider *volumeViewSlider;
        for (int i =0; i<2; i++) {
            for (UIView *view in [volumeController subviews]){
                if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
                    volumeViewSlider = (UISlider *) view;
                }
            }
            [volumeViewSlider  setValue:0.5];
            
            if (windows.count > 0) {
                [[windows objectAtIndex:0] addSubview:volumeController];
            }
        }

        
        LeftEquilizerView.backgroundColor = [UIColor clearColor];
        NowPlayingProgramNameLabel.font=[UIFont fontWithName:@"Arimo-Bold" size:NowPlayingProgramNameLabel.font.pointSize];
        NowPlayingTitleLabel.font=[UIFont fontWithName:@"Arimo" size:NowPlayingTitleLabel.font.pointSize];
        
        // Waveform color
        self.LeftEquilizerShapeLayer = [CAShapeLayer layer];
        self.LeftEquilizerShapeLayer.path = [[self LeftPathAtInterval:100] CGPath];
        self.LeftEquilizerShapeLayer.fillColor = [[UIColor clearColor] CGColor];
        self.LeftEquilizerShapeLayer.lineWidth = 2;
        self.LeftEquilizerShapeLayer.strokeColor = [[UIColor colorWithRed:167/255.0f green:183/255.0f blue:183/255.0f alpha:1] CGColor];
        [LeftEquilizerView.layer addSublayer:self.LeftEquilizerShapeLayer];
        
        self.SeconedLeftEquilizerShapeLayer = [CAShapeLayer layer];
        self.SeconedLeftEquilizerShapeLayer.path = [[self LeftPathAtInterval:100] CGPath];
        self.SeconedLeftEquilizerShapeLayer.fillColor = [[UIColor clearColor] CGColor];
        self.SeconedLeftEquilizerShapeLayer.lineWidth = 1.6;
        self.SeconedLeftEquilizerShapeLayer.strokeColor = [[UIColor colorWithRed:156/255.0f green:72/255.0f blue:65/255.0f alpha:1] CGColor];
        [SeconedLeftEquilizerView.layer addSublayer:self.SeconedLeftEquilizerShapeLayer];
        
        self.RightEquilizerShapeLayer = [CAShapeLayer layer];
        self.RightEquilizerShapeLayer.path = [[self LeftPathAtInterval:100] CGPath];
        self.RightEquilizerShapeLayer.fillColor = [[UIColor clearColor] CGColor];
        self.RightEquilizerShapeLayer.lineWidth = 2;
        self.RightEquilizerShapeLayer.strokeColor = [[UIColor colorWithRed:167/255.0f green:183/255.0f blue:183/255.0f alpha:1] CGColor];
        [RightEquilizerView.layer addSublayer:self.RightEquilizerShapeLayer];
        
        self.SeconedRightEquilizerShapeLayer = [CAShapeLayer layer];
        self.SeconedRightEquilizerShapeLayer.path = [[self LeftPathAtInterval:100] CGPath];
        self.SeconedRightEquilizerShapeLayer.fillColor = [[UIColor clearColor] CGColor];
        self.SeconedRightEquilizerShapeLayer.lineWidth = 1.6;
        self.SeconedRightEquilizerShapeLayer.strokeColor = [[UIColor colorWithRed:156/255.0f green:72/255.0f blue:65/255.0f alpha:1] CGColor];
        [SeconedRightEquilizerView.layer addSublayer:self.SeconedRightEquilizerShapeLayer];
        
        CGFloat InitialVolumeSliderControlSize = VolumeView.frame.size.height;
        self.VolumeSliderControl = [[DCKnob alloc] initWithDelegate:self] ;
        self.VolumeSliderControl.frame = CGRectMake(floorf((VolumeView.frame.size.width - InitialVolumeSliderControlSize) / 2),
                                                    floorf((VolumeView.frame.size.height - InitialVolumeSliderControlSize) / 2),
                                                    InitialVolumeSliderControlSize,
                                                    InitialVolumeSliderControlSize);
        
        
        self.VolumeSliderControl.color = [UIColor colorWithRed:218/255.0f green:178/255.0f blue:71/255.0f alpha:1];;
        self.VolumeSliderControl.backgroundColor = [UIColor clearColor];
        self.VolumeSliderControl.min = 0;
        self.VolumeSliderControl.max = 100;
        self.VolumeSliderControl.value = 30;
        self.VolumeSliderControl.valueArcWidth=10;
        self.VolumeSliderControl.cutoutSize = 180;
        self.VolumeSliderControl.arcStartAngle =90;
        
        
        [VolumeView addSubview:self.VolumeSliderControl];
        NowPlayingTitleLabel.text =@"";
        NowPlayingProgramNameLabel.text=@"";
        [ShowImageView setHidden:YES];
       
        if (!InternetConnection) {
            NowPlayingLabel.textColor = [UIColor blackColor];
            NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
            NowPlayingLabel.text =@"FAILED TO CONNECT";
    
        }
        else{
            NowPlayingLabel.textColor = [UIColor blackColor];
            NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
            NowPlayingLabel.text =@"READY TO PLAY";
        }
        
        NowPlayingProgramNameLabel.marqueeType = MLContinuousReverse;
        NowPlayingProgramNameLabel.scrollDuration = 8.0;
        NowPlayingProgramNameLabel.fadeLength = 15.0f;
        
        NowPlayingTitleLabel.marqueeType = MLContinuousReverse;
        NowPlayingTitleLabel.scrollDuration = 8.0;
        NowPlayingTitleLabel.fadeLength = 15.0f;

        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark Equilizer methods
- (void)UpdateEquilizerLevels:(NSTimer *)timer {
    @try {
        if (streamer.isPlaying) {
            [NowPlayingTitleLabel setHidden:NO];
            [NowPlayingProgramNameLabel setHidden:NO];
            [ShowImageView setHidden:NO];
            float *array;
            NSLog(@"%f",[streamer averagePowerForChannel:1]);
            if ([streamer averagePowerForChannel:0]>0) {
                array = (float *)malloc([streamer averagePowerForChannel:1]);
                LeftChannel =[streamer averagePowerForChannel:0];
                self.LeftEquilizerShapeLayer.path = [[self LeftPathAtInterval:[streamer averagePowerForChannel:0]] CGPath];
                self.SeconedLeftEquilizerShapeLayer.path = [[self SeconedLeftPathAtInterval:[streamer averagePowerForChannel:0]] CGPath];
                RightChannel =[streamer averagePowerForChannel:1];
                self.RightEquilizerShapeLayer.path = [[self RightPathAtInterval:[streamer averagePowerForChannel:1]] CGPath];
                self.SeconedRightEquilizerShapeLayer.path = [[self SeconedRightPathAtInterval:[streamer averagePowerForChannel:1]] CGPath];
            }
        }
        else{
            self.RightEquilizerShapeLayer.path = [[self RightPathAtInterval:0] CGPath];
            self.LeftEquilizerShapeLayer.path= [[self LeftPathAtInterval:0] CGPath];
            self.SeconedLeftEquilizerShapeLayer.path= [[self LeftPathAtInterval:0] CGPath];
            self.SeconedRightEquilizerShapeLayer.path= [[self LeftPathAtInterval:0] CGPath];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
- (BOOL)canBecomeFirstResponder {
	return YES;
}
-(void)viewDidAppear:(BOOL)animated
{
    @try {
        [super viewDidAppear:animated];
        self.navigationController.navigationBarHidden=YES;
        
        UIApplication *application = [UIApplication sharedApplication];
        if([application respondsToSelector:@selector(beginReceivingRemoteControlEvents)])
            [application beginReceivingRemoteControlEvents];
        [self becomeFirstResponder]; // this enables listening for events
        
        // update the UI in case we were in the background
        NSNotification *notification =
        [NSNotification
         notificationWithName:ASStatusChangedNotification
         object:self];
        
        [[NSNotificationCenter defaultCenter]
         postNotification:notification];
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark Drow Equilizer methods
- (UIBezierPath *)SeconedLeftPathAtInterval:(NSTimeInterval) interval
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    @try {
        
        [path moveToPoint:CGPointMake(0,LeftEquilizerView.bounds.size.height / 2.0)];
        
        CGFloat fractionOfSecond = interval - floor(interval);
        
        CGFloat yOffset = LeftEquilizerView.bounds.size.height * sin(fractionOfSecond * M_PI * LeftChannel*20);
        
        [path addCurveToPoint:CGPointMake(SeconedLeftEquilizerView.bounds.size.width, SeconedLeftEquilizerView.bounds.size.height / 2.0)
                controlPoint1:CGPointMake(SeconedLeftEquilizerView.bounds.size.width / 2.0, SeconedLeftEquilizerView.bounds.size.height / 2.0 - yOffset*1.7)
                controlPoint2:CGPointMake(SeconedLeftEquilizerView.bounds.size.width / 2.0, SeconedLeftEquilizerView.bounds.size.height / 2.0 + yOffset*1.7)
         ];
        
    }
    @catch (NSException *exception) {
        
    }
    
    return path;
}

- (UIBezierPath *)LeftPathAtInterval:(NSTimeInterval) interval
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    @try {
        [path moveToPoint:CGPointMake(0,LeftEquilizerView.bounds.size.height / 2.0)];
        
        CGFloat fractionOfSecond = interval - floor(interval);
        
        CGFloat yOffset = LeftEquilizerView.bounds.size.height * sin(fractionOfSecond * M_PI * LeftChannel*20);
        
        [path addCurveToPoint:CGPointMake(LeftEquilizerView.bounds.size.width, LeftEquilizerView.bounds.size.height / 2.0)
                controlPoint1:CGPointMake(LeftEquilizerView.bounds.size.width / 2.0, LeftEquilizerView.bounds.size.height / 2.0 - yOffset*2.1)
                controlPoint2:CGPointMake(LeftEquilizerView.bounds.size.width / 2.0, LeftEquilizerView.bounds.size.height / 2.0 + yOffset*2.1)
         ];
        
    }
    @catch (NSException *exception) {
        
    }
    
    return path;
}
- (UIBezierPath *)RightPathAtInterval:(NSTimeInterval) interval
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    @try {
        [path moveToPoint:CGPointMake(0,RightEquilizerView.bounds.size.height / 2.0)];
        
        CGFloat fractionOfSecond = interval - floor(interval);
        
        CGFloat yOffset = RightEquilizerView.bounds.size.height * sin(fractionOfSecond * M_PI * LeftChannel*20);
        
        [path addCurveToPoint:CGPointMake(RightEquilizerView.bounds.size.width, RightEquilizerView.bounds.size.height / 2.0)
                controlPoint1:CGPointMake(RightEquilizerView.bounds.size.width / 2.0, RightEquilizerView.bounds.size.height / 2.0 - yOffset*2.1)
                controlPoint2:CGPointMake(RightEquilizerView.bounds.size.width / 2.0, RightEquilizerView.bounds.size.height / 2.0 + yOffset*2.1)
         ];
    }
    @catch (NSException *exception) {
        
    }
    
    return path;
}

- (UIBezierPath *)SeconedRightPathAtInterval:(NSTimeInterval) interval
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    @try {
        
        [path moveToPoint:CGPointMake(0,RightEquilizerView.bounds.size.height / 2.0)];
        
        CGFloat fractionOfSecond = interval - floor(interval);
        
        CGFloat yOffset = SeconedRightEquilizerView.bounds.size.height * sin(fractionOfSecond * M_PI * LeftChannel*20);
        
        [path addCurveToPoint:CGPointMake(SeconedRightEquilizerView.bounds.size.width, SeconedRightEquilizerView.bounds.size.height / 2.0)
                controlPoint1:CGPointMake(SeconedRightEquilizerView.bounds.size.width / 2.0, SeconedRightEquilizerView.bounds.size.height / 2.0 - yOffset*1.7)
                controlPoint2:CGPointMake(SeconedRightEquilizerView.bounds.size.width / 2.0, SeconedRightEquilizerView.bounds.size.height / 2.0 + yOffset*1.7)
         ];
        
    }
    @catch (NSException *exception) {
        
    }
    
    return path;
}
#pragma -mark View Controller Actions
- (IBAction)ShareAction:(UIButton *)sender {
    @try {
        NSURL *url = [NSURL URLWithString:@"http://mind-radio.weebly.com/"];
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[@"Mind Radio App", url]
         applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)PlayButtonAction:(UIButton *)sender {
    @try {
        
       [ShowImageView setHidden:YES];
        NowPlayingLabel.textColor = [UIColor blackColor];
        NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
        NowPlayingLabel.text =@"READY TO PLAY";
        [NowPlayingProgramNameLabel setHidden:YES];
        [NowPlayingTitleLabel setHidden:YES];
        
        if (!InternetConnection) {
            NowPlayingLabel.text =@"FAILED TO CONNECT";
            return;
        }
        if (IS_PLAY) {
         
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            if (streamer) {
                if ([streamer isPlaying]) {
                    [streamer pause];
                    NSLog(@"pause clicked");
                }
                else if ([streamer isWaiting])
                {
                     NSLog(@"waiting pause clicked --- destroy");
                    [self destroyStreamer];
                    IS_PLAY =YES;
                    [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                    [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                }
            }
            
            IS_PLAY =NO;
            NowPlayingLabel.text =@"READY TO PLAY";
            [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
            [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
        }
        else{
            if (!streamer) {
                 NSLog(@" play clicked --- create new");
                 [NSThread detachNewThreadSelector:@selector(GetAudioPlayerURL) toTarget:self withObject:nil];
                   IS_PLAY =YES;
            }
            else{
                if ([streamer isWaiting]) {
                    NSLog(@"waiting play clicked --- destroy");
                    [self destroyStreamer];
                    IS_PLAY =NO;
                    [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                    [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                }
                else{
                      NSLog(@"waiting play clicked --- start");
                   [streamer start];
                    IS_PLAY =YES;
                    [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
                    [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
                    [NowPlayingProgramNameLabel setHidden:NO];
                    [NowPlayingTitleLabel setHidden:NO];

                }
            }
         
        }
    }
    @catch (NSException *exception) {
       
    }
}

- (IBAction)MuteAction:(UIButton *)sender {
    @try {
        soundLevel-=0.1;
        if (soundLevel<=0) {
            soundLevel=0;
        }
        self.VolumeSliderControl.value=[[NSString stringWithFormat:@"%f.00f",soundLevel]floatValue] *100;
        NSArray *windows = [UIApplication sharedApplication].windows;
        volumeController.alpha = 0.1f;
        UISlider *volumeViewSlider;
        
        for (UIView *view in [volumeController subviews]){
            if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
                volumeViewSlider = (UISlider *) view;
            }
        }
        
        [volumeViewSlider  setValue:(float)soundLevel];
        volumeController.userInteractionEnabled = NO;
        
        if (windows.count > 0) {
            [[windows objectAtIndex:0] addSubview:volumeController];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)FullVolumAction:(UIButton *)sender {
    @try {
        soundLevel+=0.1;
        if (soundLevel>=1) {
            soundLevel=1;
        }
        self.VolumeSliderControl.value=[[NSString stringWithFormat:@"%f.00f",soundLevel]floatValue] *100;
        NSArray *windows = [UIApplication sharedApplication].windows;
        volumeController.alpha = 0.1f;
        UISlider *volumeViewSlider;
        
        for (UIView *view in [volumeController subviews]){
            if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
                volumeViewSlider = (UISlider *) view;
            }
        }
        
        [volumeViewSlider  setValue:(float)soundLevel];
        volumeController.userInteractionEnabled = NO;
        
        if (windows.count > 0) {
            [[windows objectAtIndex:0] addSubview:volumeController];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark Volume Control Notification Change
- (void)volumeChanged:(NSNotification *)notification
{
    @try {
        soundLevel =
        [[[notification userInfo]
          objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
         floatValue];
        self.VolumeSliderControl.value=[[NSString stringWithFormat:@"%0.2f",soundLevel]floatValue] *100;
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark - Internet Reachability
- (void)internetReachabilityChanged:(NSNotification *)note
{
    @try {
        NetworkStatus ns = [(Reachability *)[note object] currentReachabilityStatus];
        [ShowImageView setHidden:YES];
        [NowPlayingTitleLabel setHidden:YES];
        [NowPlayingProgramNameLabel setHidden:YES];
        NowPlayingLabel.textColor = [UIColor blackColor];
        NowPlayingLabel.font = [UIFont boldSystemFontOfSize:17];
        
        if (ns == NotReachable)
        {
            if (InternetConnection)
            {
                InternetConnection = NO;
                NowPlayingLabel.text =@"FAILED TO CONNECT";
                  //IS_PLAY = NO;
//                [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
//                [PlayImageView setImage:[UIImage imageNamed:@"pause_ic.png"] ];
            }
        }
        else
        {
            if (!InternetConnection)
            {
                InternetConnection = YES;
                NowPlayingLabel.text =@"READY TO PLAY";
               // IS_PLAY = NO;
//                [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
//                [PlayImageView setImage:[UIImage imageNamed:@"play_btn.png"] ];
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark Remote Control Events
/* The iPod controls will send these events when the app is in the background */
- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
	switch (event.subtype) {
		case UIEventSubtypeRemoteControlTogglePlayPause:
			[streamer pause];
			break;
		case UIEventSubtypeRemoteControlPlay:
			[streamer start];
			break;
		case UIEventSubtypeRemoteControlPause:
			[streamer pause];
			break;
		case UIEventSubtypeRemoteControlStop:
			[streamer stop];
			break;
		default:
			break;
	}
}
@end
