//
//  MenuViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/27/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "LibrariesHeader.h"
#import "SchedualViewController.h"
#import "StreamingViewController.h"
#import "HostsViewController.h"
#import "AboutViewController.h"
#import "ShowsViewController.h"
#import "MyAlertsViewController.h"

const int  SCHEDULE_INDEX =0;
const int  HOSTS_INDEX =1;
const int  SHOWS_INDEX =2;
const int  MY_ALERTS_INDEX =3;
const int  ABOUT_INDEX =4;
@interface MenuViewController ()
{
    NSArray *MenuOptions;
}
@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{

}
- (void)viewDidLoad
{

    [super viewDidLoad];
    MenuOptions = [[NSArray alloc]init];
    MenuOptions = [[Language getObject] getArrayWithKey:@"MenuOptions"];
    [MenuTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell;
    @try {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
        if (cell==nil) {
            cell =(MenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:nil options:nil]objectAtIndex:0];
        }
        if (indexPath.row == 0) {
            [cell.DayLabel setHidden:NO];
            NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"dd"];
            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
             cell.DayLabel.text = [formatter stringFromDate:[NSDate date]];
        }
        cell.ItemTitleLabel.font = [UIFont fontWithName:@"Arimo" size:cell.ItemTitleLabel.font.pointSize];
        cell.ItemImageView.image = [UIImage imageNamed:[[MenuOptions objectAtIndex:indexPath.row]objectForKey:@"image"]];
        cell.ItemTitleLabel.text = [[MenuOptions objectAtIndex:indexPath.row]objectForKey:@"option"];
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [MenuOptions count];
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        UIViewController *pushedViewController;
        switch (indexPath.row) {
            case SCHEDULE_INDEX:
            {
                pushedViewController = [[SchedualViewController alloc]init];
            }
                break;
            case HOSTS_INDEX:
            {
                pushedViewController = [[HostsViewController alloc]init];
            }
                break;
            case SHOWS_INDEX:
            {
                pushedViewController = [[ShowsViewController alloc]init];

            }
                break;
            case MY_ALERTS_INDEX:
            {
                 pushedViewController = [[MyAlertsViewController alloc]init];
            }
                break;
            case ABOUT_INDEX:
            {
                 pushedViewController = [[AboutViewController alloc]init];
            }
                break;
                
        }
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSMutableArray *controllersList = [[NSMutableArray alloc]initWithArray:navigationController.viewControllers];
        int index = 0;
        for (UIViewController *controller in controllersList) {
            if ([controller class]!=[StreamingViewController class]) {
                [controllersList removeObjectAtIndex:index];
            }
            index++;
        }
        [controllersList  addObject:pushedViewController];
        navigationController.viewControllers = controllersList ;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    @catch (NSException *exception) {
        
    }
   
}
- (IBAction)LogoButtonAction:(UIButton *)sender {
    @try {
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        [navigationController popToRootViewControllerAnimated:NO];
         [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
