//
//  MenuViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/27/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UITableView *MenuTableView;
}
- (IBAction)LogoButtonAction:(UIButton *)sender;
@end
