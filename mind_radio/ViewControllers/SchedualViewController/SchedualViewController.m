//
//  SchedualViewController.m
//  kau
//
//  Created by Assem Imam on 7/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
#import "SchedualViewController.h"
#import "SchedualCell.h"

@interface SchedualViewController ()
{
    NSMutableArray *schedualList;
    NSMutableArray*daysList;
    int currentDayIndex;
    NSArray *timeList;
    int selectedShowIndex;
    int selectedAlermValue;
    BOOL fromCashedData;
    BOOL AlermIsWeekly;
}
@end

@implementation SchedualViewController
-(BOOL)CheckAlarmForShow:(NSString*)showID
{
    BOOL isExist =NO;
    @try {
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";

         id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObject:fldShowId] Tables:[NSArray arrayWithObjects:@"my_alerts", nil] Where:[NSString stringWithFormat:@"ShowId = %i",[showID intValue]] FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:NO];
        if (showsResut) {
            if ([showsResut count]>0) {
                isExist = YES;
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    return isExist;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    
    NextButton.transform = CGAffineTransformMakeRotation(M_PI);
    selectedAlermValue = 5;
    selectedShowIndex=-1;
    fromCashedData=NO;
    AlermIsWeekly=YES;
    timeList = [[NSArray alloc]initWithObjects:@"5 Mins",@"10 Mins",@"15 Mins", nil];
    [TimePicker reloadAllComponents];
   
    LoadingLabel.font=[UIFont fontWithName:@"Arimo-Bold" size:LoadingLabel.font.pointSize];
    DayNameLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:DayNameLabel.font.pointSize];
    daysList = [[NSMutableArray alloc]init];
    
    [self.view bringSubviewToFront:LoadingView];
    if (InternetConnection) {
         [NSThread detachNewThreadSelector:@selector(GetSchedual) toTarget:self withObject:nil];
    }
    else{
         id result=  [self GetCashedShows];
         [self HandleCashShowsResult:result];
        if (result) {
            if ([result count]==0 ) {
                [LoadingActivityIndicator stopAnimating];
                [self.view sendSubviewToBack:LoadingView];
                [LoadingView setHidden:YES];
            }
        }
        else{
            [LoadingActivityIndicator stopAnimating];
            [self.view sendSubviewToBack:LoadingView];
            [LoadingView setHidden:YES];
        }
        
    }
   
}
-(void)PerformNextAnimation
{
    @try {
        [SchedualTableView setHidden:YES];
        [SchedualTableView  setFrame:CGRectMake(SchedualTableView.frame.size.width, SchedualTableView.frame.origin.y, SchedualTableView.frame.size.width, SchedualTableView.frame.size.height)];
        [UIView animateWithDuration:0.2 animations:^{
            [SchedualTableView setHidden:NO];
            [SchedualTableView  setFrame:CGRectMake(0, SchedualTableView.frame.origin.y, SchedualTableView.frame.size.width, SchedualTableView.frame.size.height)];
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)PerformPreviousAnimation
{
    @try {
        [SchedualTableView setHidden:YES];
        [SchedualTableView  setFrame:CGRectMake(-1* SchedualTableView.frame.size.width, SchedualTableView.frame.origin.y, SchedualTableView.frame.size.width, SchedualTableView.frame.size.height)];
        [UIView animateWithDuration:0.2 animations:^{
            [SchedualTableView setHidden:NO];
            [SchedualTableView  setFrame:CGRectMake(0, SchedualTableView.frame.origin.y, SchedualTableView.frame.size.width, SchedualTableView.frame.size.height)];
        }];
    }
    @catch (NSException *exception) {
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(id)GetCashedShows
{
    fromCashedData=YES;
    NSMutableDictionary *ShowDisctionary = [[NSMutableDictionary alloc]init];
    @try {
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";
        
        DB_Field  *fldDayName = [[DB_Field alloc]init];
        fldDayName.FieldName=@"DayName";
        
        DB_Field  *fldDayOfWeek= [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
        
        DB_Field  *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
        fldStartTimeStr.FieldName=@"StartTimeStr";
        
        DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
        fldEndTimeStr.FieldName=@"EndTimeStr";
        
        DB_Field  *fldTimeZone = [[DB_Field alloc]init];
        fldTimeZone.FieldName=@"TimeZone";
        fldTimeZone.FieldAlias=@"TimeZoneOffset";
        
        DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
        fldThumbNailURL.FieldName=@"ThumbNailURL";
        
        DB_Field  *fldDescriptione = [[DB_Field alloc]init];
        fldDescriptione.FieldName=@"Description";
        
        id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldDayName,fldDayOfWeek,fldDescriptione,fldStartTimeStr,fldEndTimeStr,fldThumbNailURL,fldTitle,fldTimeZone,nil] Tables:[NSArray arrayWithObject:@"shows"] Where:nil FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
        NSMutableArray *showsArray = [[NSMutableArray alloc]init];
        
        
        if (showsResut) {
            if ([showsResut count]> 0) {
                
                for (id show in showsResut) {
                    NSMutableDictionary *showsDic = [show mutableCopy];
                    DB_Field  *fldHostId = [[DB_Field alloc]init];
                    fldHostId.FieldName=@"id";
                    
                    DB_Field  *fldHostName = [[DB_Field alloc]init];
                    fldHostName.FieldName=@"Name";
                    
                    id hostsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldHostId,fldHostName,nil] Tables:[NSArray arrayWithObject:@"show_hosts"] Where:[NSString stringWithFormat:@"ShowId=%@",[show objectForKey:@"ShowId"]] FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
                    if (hostsResut) {
                        NSMutableArray *hostsArray = [[NSMutableArray alloc]init];
                        for (id host in hostsResut) {
                            
                            if ([host objectForKey:@"Name"] && ![[host objectForKey:@"Name"] isEqualToString:@"null"]&& [NSNull null]!= [host objectForKey:@"Name"]) {
                                [hostsArray addObject:host];
                            }
                        }
                        
                        [showsDic setObject:hostsArray forKey:@"Hosts"];
                    }
                    
                    [showsArray addObject:showsDic];
                }
                
                NSPredicate *saturdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Saturday'"];
                id saturdayShowsList = [showsArray filteredArrayUsingPredicate:saturdayPridicate];
                [ShowDisctionary setObject:saturdayShowsList forKey:@"Saturday"];
                
                NSPredicate *sundayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Sunday'"];
                id sundayShowsList = [showsArray filteredArrayUsingPredicate:sundayPridicate];
                [ShowDisctionary setObject:sundayShowsList forKey:@"Sunday"];
                
                NSPredicate *mondayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Monday'"];
                id mondayShowsList = [showsArray filteredArrayUsingPredicate:mondayPridicate];
                [ShowDisctionary setObject:mondayShowsList forKey:@"Monday"];
                
                NSPredicate *tusedayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Tuseday'"];
                id tusedayShowsList = [showsArray filteredArrayUsingPredicate:tusedayPridicate];
                [ShowDisctionary setObject:tusedayShowsList forKey:@"Tuseday"];
                
                NSPredicate *wednesdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Wednesday'"];
                id wednesdayShowsList = [showsArray filteredArrayUsingPredicate:wednesdayPridicate];
                [ShowDisctionary setObject:wednesdayShowsList forKey:@"Wednesday"];
                
                NSPredicate *thursdayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Thursday'"];
                id thursdayShowsList = [showsArray filteredArrayUsingPredicate:thursdayPridicate];
                [ShowDisctionary setObject:thursdayShowsList forKey:@"Thursday"];
                
                NSPredicate *fridayPridicate = [NSPredicate predicateWithFormat:@"DayName=='Friday'"];
                id fridayShowsList = [showsArray filteredArrayUsingPredicate:fridayPridicate];
                [ShowDisctionary setObject:fridayShowsList forKey:@"Friday"];
                
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
    return ShowDisctionary;
}
-(void)HandleCashShowsResult:(id)result
{
    @try {
        if ([result count]>0) {
                daysList = [[NSMutableArray alloc]init];
                NSMutableDictionary *saturday = [[NSMutableDictionary alloc]init];
                [saturday setObject:@"Saturday" forKey:@"name"];
                [saturday setObject:[result objectForKey:@"Saturday"] forKey:@"shows_list"];
                
                NSMutableDictionary *sunday = [[NSMutableDictionary alloc]init];
                [sunday setObject:@"Sunday" forKey:@"name"];
                [sunday setObject:[result objectForKey:@"Sunday"] forKey:@"shows_list"];
                
                NSMutableDictionary *monday = [[NSMutableDictionary alloc]init];
                [monday setObject:@"Monday" forKey:@"name"];
                [monday setObject:[result objectForKey:@"Monday"] forKey:@"shows_list"];
                
                NSMutableDictionary *tuseday = [[NSMutableDictionary alloc]init];
                [tuseday setObject:@"Tuseday" forKey:@"name"];
                [tuseday setObject:[result objectForKey:@"Tuseday"] forKey:@"shows_list"];
                
                NSMutableDictionary *wednesday = [[NSMutableDictionary alloc]init];
                [wednesday setObject:@"Wednesday" forKey:@"name"];
                [wednesday setObject:[result objectForKey:@"Wednesday"] forKey:@"shows_list"];
                
                NSMutableDictionary *thursday = [[NSMutableDictionary alloc]init];
                [thursday setObject:@"Thursday" forKey:@"name"];
                [thursday setObject:[result objectForKey:@"Thursday"] forKey:@"shows_list"];
                
                NSMutableDictionary *friday = [[NSMutableDictionary alloc]init];
                [friday setObject:@"Friday" forKey:@"name"];
                [friday setObject:[result objectForKey:@"Friday"] forKey:@"shows_list"];
                
            
                [daysList addObject:sunday];
                [daysList addObject:monday];
                [daysList addObject:tuseday];
                [daysList addObject:wednesday];
                [daysList addObject:thursday];
                [daysList addObject:friday];
                [daysList addObject:saturday];
        }
        else{
            if (!InternetConnection && fromCashedData) {
                [SchedualTableView setHidden:YES];
                [TryAgainView setHidden:NO];
                [self.view bringSubviewToFront:TryAgainView];
            }

        }
        
        
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"E"];
        dateFormatter.timeZone=[NSTimeZone localTimeZone];
        dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSCalendar *gregorian = [NSCalendar currentCalendar];
        [gregorian setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"en_US"]];
        gregorian.timeZone=[NSTimeZone localTimeZone];
        NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
        currentDayIndex = [comps weekday];
       
        DayNameLabel.text =[[daysList objectAtIndex:[comps weekday]-1]objectForKey:@"name"];
        schedualList  = [[daysList objectAtIndex:[comps weekday]-1]objectForKey:@"shows_list"];
        [SchedualTableView reloadData];
        
        [LoadingActivityIndicator stopAnimating];
        [self.view sendSubviewToBack:LoadingView];
        [LoadingView setHidden:YES];
        if ([schedualList count]>0) {
            [SchedualTableView setHidden:NO];
            [NoDataView setHidden:YES];
            [self.view sendSubviewToBack:NoDataView];
        }
        else{
            [SchedualTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)CashShows:(id)result
{
    @try {
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        if ([NSNull null]!= [result objectForKey:@"Saturday"]) {
            for (id show in [result objectForKey:@"Saturday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Saturday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Sunday"]) {
            for (id show in [result objectForKey:@"Sunday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Sunday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Monday"]) {
            for (id show in [result objectForKey:@"Monday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Monday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Tuseday"]) {
            for (id show in [result objectForKey:@"Tuseday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Tuseday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Wednesday"]) {
            for (id show in [result objectForKey:@"Wednesday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Wednesday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
                
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Thursday"]) {
            for (id show in [result objectForKey:@"Thursday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Thursday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        if ([NSNull null]!= [result objectForKey:@"Friday"]) {
            for (id show in [result objectForKey:@"Friday"]) {
                
                DB_Field  *fldShowId = [[DB_Field alloc]init];
                fldShowId.FieldName=@"ShowId";
                fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                fldShowId.IS_PRIMARY_KEY=YES;
                fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                
                DB_Field  *fldDayName = [[DB_Field alloc]init];
                fldDayName.FieldName=@"DayName";
                fldDayName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayName.FieldValue=@"Friday";
                
                DB_Field  *fldDayOfWeek = [[DB_Field alloc]init];
                fldDayOfWeek.FieldName=@"DayOfWeek";
                fldDayOfWeek.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDayOfWeek.FieldValue=[NSNull null]!=[show objectForKey:@"DayOfWeek"]?[show objectForKey:@"DayOfWeek"]:@"";
                
                
                DB_Field  *fldTitle = [[DB_Field alloc]init];
                fldTitle.FieldName=@"Title";
                fldTitle.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTitle.FieldValue=[NSNull null]!=[show objectForKey:@"Title"]?[show objectForKey:@"Title"]:@"";
                
                
                DB_Field  *fldStartTimeStr = [[DB_Field alloc]init];
                fldStartTimeStr.FieldName=@"StartTimeStr";
                fldStartTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldStartTimeStr.FieldValue=[show objectForKey:@"StartTimeStr"];
                
                DB_Field  *fldEndTimeStr = [[DB_Field alloc]init];
                fldEndTimeStr.FieldName=@"EndTimeStr";
                fldEndTimeStr.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldEndTimeStr.FieldValue=[show objectForKey:@"EndTimeStr"];
                
                DB_Field  *fldTimeZone = [[DB_Field alloc]init];
                fldTimeZone.FieldName=@"TimeZone";
                fldTimeZone.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldTimeZone.FieldValue=[NSNull null]!=[show objectForKey:@"TimeZoneOffset"]?[show objectForKey:@"TimeZoneOffset"]:@"";
                
                DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
                fldThumbNailURL.FieldName=@"ThumbNailURL";
                fldThumbNailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                fldThumbNailURL.FieldValue=[NSNull null]!=[show objectForKey:@"ThumbNailURL"]?[show objectForKey:@"ThumbNailURL"]:@"";
                
                DB_Field  *fldDescriptione = [[DB_Field alloc]init];
                fldDescriptione.FieldName=@"Description";
                fldDescriptione.FieldDataType=FIELD_DATA_TYPE_TEXT;
                fldDescriptione.FieldValue=[NSNull null]!=[show objectForKey:@"Description"]?[show objectForKey:@"Description"]:@"";
                
                
                DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
                showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldDayName,fldTitle,fldStartTimeStr,fldEndTimeStr,fldTimeZone,fldDescriptione,fldThumbNailURL,fldDayOfWeek, nil];
                
                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"shows"];
                if (isSuccess) {
                    if ([NSNull null]!= [show objectForKey:@"Hosts"]) {
                        for (id host in [show objectForKey:@"Hosts"]) {
                            if ([host objectForKey:@"HostId"]) {
                                
                                DB_Field  *fldHostShowId = [[DB_Field alloc]init];
                                fldHostShowId.FieldName=@"ShowId";
                                fldHostShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostShowId.IS_PRIMARY_KEY=YES;
                                fldHostShowId.FieldValue=[NSString stringWithFormat:@"%i",[[show objectForKey:@"ShowId"]intValue]];
                                NSString *hostID =[NSString stringWithFormat:@"%i",[[host objectForKey:@"HostId"]intValue]];;
                                DB_Field  *fldHostId = [[DB_Field alloc]init];
                                fldHostId.FieldName=@"id";
                                fldHostId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
                                fldHostId.IS_PRIMARY_KEY=YES;
                                fldHostId.FieldValue=hostID;
                                
                                NSString *hostName = [NSNull null]!=[host objectForKey:@"Name"]&& [host objectForKey:@"Name"] &&![[host objectForKey:@"Name"] isEqualToString:@"null" ]?[host objectForKey:@"Name"]:@"";
                                DB_Field  *fldHostName = [[DB_Field alloc]init];
                                fldHostName.FieldName=@"Name";
                                fldHostName.FieldDataType=FIELD_DATA_TYPE_TEXT;
                                fldHostName.FieldValue=hostName;
                                
                                NSString *hostImage = [NSNull null]!=[host objectForKey:@"ThumbnailURL"]&& [host objectForKey:@"ThumbnailURL"] &&![[host objectForKey:@"ThumbnailURL"] isEqualToString:@"null" ]?[host objectForKey:@"ThumbnailURL"]:@"";
                                DB_Field  *fldThumbnailURL = [[DB_Field alloc]init];
                                fldThumbnailURL.FieldName=@"ThumbnailURL";
                                fldThumbnailURL.FieldDataType=FIELD_DATA_TYPE_IMAGE;
                                fldThumbnailURL.FieldValue=hostImage;
                                
                                DB_Recored *showHostRecored = [[DB_Recored alloc]init];
                                showHostRecored.Fields = [NSMutableArray  arrayWithObjects:fldHostShowId,fldHostId,fldHostName,fldThumbnailURL, nil];
                                
                                int isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showHostRecored] inTable:@"show_hosts"];
                            }
                            
                            
                        }
                    }
                }
            }
            
        }
        [self performSelectorOnMainThread:@selector(HandleCashShowsResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
    
}


-(void)GetSchedual
{
    @try {
        fromCashedData=NO;
        id result = [Webservice GetShows];
        [self performSelectorOnMainThread:@selector(HandleGetSchedualResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)HandleGetSchedualResult:(id)result
{
    @try {
        if (result) {
            [NSThread detachNewThreadSelector:@selector(CashShows:) toTarget:self withObject:result];
        }
        else{
            id result=  [self GetCashedShows];
            [self HandleCashShowsResult:result];
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark uitableview datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [schedualList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SchedualCell*cell;
    @try {
        cell =(SchedualCell*)[tableView dequeueReusableCellWithIdentifier:@"cleSchedual"];
        if (cell==nil) {
            cell = (SchedualCell*)[[[NSBundle mainBundle]loadNibNamed:@"SchedualCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSString *imageUrlString = [NSNull null]!=[[schedualList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"]?[[schedualList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"]:@"";
        
        NSURL *imageUrl = [NSURL URLWithString:[imageUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if (fromCashedData) {
            [cell.LoadingActivityIndicator startAnimating];
            NSString *path =[[schedualList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"];
         
            NSData *imageData =[NSData dataWithContentsOfFile:path];
            if (imageData) {
                 cell.ShowImageView.image = [UIImage imageWithData:imageData];
            }
            else{
                  cell.ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
            }
             [cell.LoadingActivityIndicator stopAnimating];
        }
        else{
            if (imageUrl) {
                [cell.LoadingActivityIndicator startAnimating];
                [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                    if (image) {
                        cell.ShowImageView.image = image;
                    }
                    else{
                        cell.ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
                    }
                    [cell.LoadingActivityIndicator stopAnimating];
                }];
                
            }
        }
        if ([self CheckAlarmForShow:[[schedualList objectAtIndex:indexPath.row]objectForKey:@"ShowId"]]) {
            cell.AlertButton.accessibilityLabel = @"1";
            [cell.AlertButton setImage:[UIImage imageNamed:@"alarmselected.png"] forState:UIControlStateNormal];
        }
        else{
            cell.AlertButton.accessibilityLabel = @"0";
            [cell.AlertButton setImage:[UIImage imageNamed:@"alarm.png"] forState:UIControlStateNormal];
        }
         cell.AlertButton.tag = indexPath.row;
        [cell.AlertButton addTarget:self action:@selector(AlertButtonAction:) forControlEvents:UIControlEventTouchUpInside];
       
//        if (fromCashedData) {
//             NSString *convertedShowStartTime =[[schedualList objectAtIndex:indexPath.row]objectForKey:@"StartTimeStr"] ;
//            NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
//            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//            dateFormatter.timeZone=[NSTimeZone localTimeZone];
//            dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
//            
//            NSDateFormatter *fullDateFormatter =[[NSDateFormatter alloc]init];
//            [fullDateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
//            fullDateFormatter.timeZone=[NSTimeZone localTimeZone];
//            fullDateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
//            
//            NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
//            [timeFormatter setDateFormat:@"h:mm a"];
//            timeFormatter.timeZone=[NSTimeZone localTimeZone];
//            timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
//            NSString *currentDateString = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:[NSDate date]],convertedShowStartTime];
//            
//            NSString*showTime = [timeFormatter stringFromDate:[fullDateFormatter dateFromString:currentDateString]];
//             cell.ProgramTimeLabel.text =showTime;
//        }
//        else{
            float  timeZoonOffset =[NSNull null]!=[[schedualList objectAtIndex:indexPath.row]objectForKey:@"TimeZoneOffset"]?[[[schedualList objectAtIndex:indexPath.row]objectForKey:@"TimeZoneOffset"]floatValue]:0;
            
            NSString *convertedShowStartTime;
            convertedShowStartTime = [self ConvertToDeviceTime:[[schedualList objectAtIndex:indexPath.row]objectForKey:@"StartTimeStr"] AndTimeOffset:timeZoonOffset];
            
            NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            dateFormatter.timeZone=[NSTimeZone localTimeZone];
            dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            
            NSDateFormatter *fullDateFormatter =[[NSDateFormatter alloc]init];
            [fullDateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
            fullDateFormatter.timeZone=[NSTimeZone localTimeZone];
            fullDateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];

            NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
            [timeFormatter setDateFormat:@"h:mm a"];
            timeFormatter.timeZone=[NSTimeZone localTimeZone];
            timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            NSString *currentDateString = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:[NSDate date]],convertedShowStartTime];
            
           NSString*showTime = [timeFormatter stringFromDate:[fullDateFormatter dateFromString:currentDateString]];
            cell.ProgramTimeLabel.text =showTime;
       // }
    
        cell.ProgramNameLabel.text =[NSNull null]!= [[schedualList objectAtIndex:indexPath.row]objectForKey:@"Title"]?[[schedualList objectAtIndex:indexPath.row]objectForKey:@"Title"]:@"";
        cell.ProgramNameLabel.font =[UIFont fontWithName:@"Arimo-Bold" size:cell.ProgramNameLabel.font.pointSize];
        cell.ProgramTimeLabel.font =[UIFont fontWithName:@"Arimo-Bold" size:cell.ProgramTimeLabel.font.pointSize];
        cell.SpeakersLabel.font =[UIFont fontWithName:@"Arimo" size:cell.SpeakersLabel.font.pointSize];
        
        NSString *hostsString=@"";
        if ([NSNull null]!=[[schedualList objectAtIndex:indexPath.row]objectForKey:@"Hosts"]) {
            for (id host in  [[schedualList objectAtIndex:indexPath.row]objectForKey:@"Hosts"]) {
                if ([host objectForKey:@"Name"] && [NSNull null]!=[host objectForKey:@"Name"] && ![[host objectForKey:@"Name"] isEqualToString:@"(null)"]) {
                    if ([[host objectForKey:@"Name"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
                        hostsString =[hostsString stringByAppendingFormat:@"%@,",[host objectForKey:@"Name"]];
                    }
                }
            }
            hostsString = [hostsString substringToIndex:[hostsString length]-1];
            cell.SpeakersLabel.text = hostsString;
        }
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(void)AlertButtonAction:(UIButton*)sender
{
    @try {
        selectedShowIndex = sender.tag;
        if ([sender.accessibilityLabel intValue]==0) {
            [v_alerm setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [v_alerm setFrame:CGRectMake(0,self.view.frame.size.height- v_alerm.frame.size.height,v_alerm.frame.size.width, v_alerm.frame.size.height)];
            }];
            [self.view bringSubviewToFront:v_alerm];
        }
        else{
            
            [[Cashing getObject] setDatabase:@"mind_radio"];
            [[Cashing getObject]DeleteDataFromTable:@"my_alerts" WhereCondition:[NSString stringWithFormat:@"ShowId=%i",[[[schedualList objectAtIndex:sender.tag]objectForKey:@"ShowId"]intValue]]];
            [(AppDelegate*) [[UIApplication sharedApplication] delegate] ScheduleShowAlerts];
            [SchedualTableView reloadData];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)PreviousButtonAction:(UIButton *)sender {
    @try {
        [self PerformPreviousAnimation];
        [NextButton setHidden:NO];
        currentDayIndex--;
        if (currentDayIndex<=0) {
            currentDayIndex = 7;
            [sender setHidden:YES];
            DayNameLabel.text =[[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"name"];
            schedualList  = [[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"shows_list"];
            currentDayIndex=0;
        }
        else
        {
            DayNameLabel.text =[[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"name"];
            schedualList  = [[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"shows_list"];
        }
        
       
        [SchedualTableView reloadData];
        
        if ([schedualList count]>0) {
            [SchedualTableView setHidden:NO];
            [NoDataView setHidden:YES];
            [self.view sendSubviewToBack:NoDataView];
        }
        else{
            [SchedualTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }

    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)NextButtonAction:(UIButton *)sender {
    @try {
        [self PerformNextAnimation];
        [PreviousButton setHidden:NO];
        currentDayIndex++;
        if (currentDayIndex>=[daysList count]-1) {
            currentDayIndex = 6;
            [sender setHidden:YES];
        }
        DayNameLabel.text =[[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"name"];
        schedualList  = [[daysList objectAtIndex:currentDayIndex-1]objectForKey:@"shows_list"];
        [SchedualTableView reloadData];
        if ([schedualList count]>0) {
            [SchedualTableView setHidden:NO];
            [NoDataView setHidden:YES];
            [self.view sendSubviewToBack:NoDataView];
        }
        else{
            [SchedualTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)AlermCancelButtonAction:(UIBarButtonItem *)sender {
    @try {

        [UIView animateWithDuration:0.3 animations:^{
            [v_alerm setFrame:CGRectMake(0,self.view.frame.size.height+ v_alerm.frame.size.height,v_alerm.frame.size.width, v_alerm.frame.size.height)];
        } completion:^(BOOL finished) {
            if (finished) {
                [self.view sendSubviewToBack:v_alerm];
                [v_alerm setHidden:YES];
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)AlermSetButtonAction:(UIBarButtonItem *)sender {
    @try {
        
        //handle show time

//        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
//        [timeFormatter setDateFormat:@"HH:mm:ss"];
//        timeFormatter.timeZone=[NSTimeZone localTimeZone];
//        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        NSString *showTime;
        
//        if (fromCashedData ) {
//            showTime =[[schedualList objectAtIndex:selectedShowIndex]objectForKey:@"StartTimeStr"];
//            
//            showTime =[timeFormatter stringFromDate:[[timeFormatter dateFromString:showTime]dateByAddingTimeInterval:-1*(selectedAlermValue * 60)]];
//        }
//        else{
            int  timeZoonOffset =[NSNull null]!=[[schedualList objectAtIndex:selectedShowIndex]objectForKey:@"TimeZoneOffset"]?[[[schedualList objectAtIndex:selectedShowIndex]objectForKey:@"TimeZoneOffset"]intValue]:0;
            float timeZoneSeconds=timeZoonOffset*3600;
            
            NSTimeZone* deviceTimeZone = [NSTimeZone localTimeZone];
            NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:timeZoneSeconds];
            NSDateFormatter *serverTimeFormatter =[[NSDateFormatter alloc]init];
            [serverTimeFormatter setDateFormat:@"HH:mm:ss"];
            serverTimeFormatter.timeZone=sourceTimeZone;
            serverTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            NSDate *serverShowTime =[serverTimeFormatter dateFromString:[[schedualList objectAtIndex:selectedShowIndex]objectForKey:@"StartTimeStr"]];
            
            NSInteger currentGMTOffset = [deviceTimeZone secondsFromGMTForDate:serverShowTime];
            NSTimeZone *convertedSourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset];
            NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
            [timeFormatter setDateFormat:@"HH:mm:ss"];
            timeFormatter.timeZone=convertedSourceTimeZone;
            timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            showTime =[timeFormatter stringFromDate:[serverShowTime dateByAddingTimeInterval:-1*(selectedAlermValue * 60)]];
       // }
        
        //cash data
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldLastTimeZoon = [[DB_Field alloc]init];
        fldLastTimeZoon.FieldName=@"LastTimeZoon";
        fldLastTimeZoon.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldLastTimeZoon.FieldValue =[NSString stringWithFormat:@"%0.1f",(float)[[NSTimeZone systemTimeZone] secondsFromGMT]/3600.0];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"ShowId";
        fldShowId.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldShowId.IS_PRIMARY_KEY=YES;
        fldShowId.FieldValue=[NSString stringWithFormat:@"%i",[[[schedualList objectAtIndex:selectedShowIndex] objectForKey:@"ShowId"]intValue]];
        
        DB_Field  *fldShowDate= [[DB_Field alloc]init];
        fldShowDate.FieldDataType =FIELD_DATA_TYPE_TEXT;
        fldShowDate.FieldName=@"ShowDate";
        fldShowDate.FieldValue=[[schedualList objectAtIndex:selectedShowIndex]objectForKey:@"StartTimeStr"];
        
        DB_Field  *fldShowTime= [[DB_Field alloc]init];
        fldShowTime.FieldName=@"ShowTime";
        fldShowTime.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldShowTime.FieldValue=showTime;
        
        DB_Field  *fldIsWeekly= [[DB_Field alloc]init];
        fldIsWeekly.FieldName=@"IsWeekly";
        fldIsWeekly.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldIsWeekly.FieldValue=AlermIsWeekly==YES?@"1":@"0";
        
        DB_Field  *fldInsertDate= [[DB_Field alloc]init];
        fldInsertDate.FieldName=@"InsertDate";
        fldInsertDate.FieldDataType=FIELD_DATA_TYPE_TEXT;
        fldInsertDate.FieldValue=[timeFormatter stringFromDate:[NSDate date]];
        
        DB_Field  *fldFired= [[DB_Field alloc]init];
        fldFired.FieldName=@"Fired";
        fldFired.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldFired.FieldValue=@"0";
        
        DB_Field  *fldRemindBefore= [[DB_Field alloc]init];
        fldRemindBefore.FieldName=@"RemindBefore";
        fldRemindBefore.FieldDataType=FIELD_DATA_TYPE_NUMBER;
        fldRemindBefore.FieldValue=[NSString stringWithFormat:@"%i",selectedAlermValue];
        
        
        DB_Recored *showAddedRecored = [[DB_Recored alloc]init];
        showAddedRecored.Fields = [NSMutableArray  arrayWithObjects:fldShowId,fldShowTime,fldInsertDate,fldIsWeekly,fldShowDate,fldFired,fldLastTimeZoon,fldRemindBefore, nil];
        
        BOOL isSuccess= [[Cashing getObject] CashData:[NSArray arrayWithObject:showAddedRecored] inTable:@"my_alerts"];
        if (isSuccess) {
            [(AppDelegate*) [[UIApplication sharedApplication] delegate] ScheduleShowAlerts];
        }
        [SchedualTableView reloadData];
        
        //perform dismiss animation
        [UIView animateWithDuration:0.3 animations:^{
            [v_alerm setFrame:CGRectMake(0,self.view.frame.size.height+ v_alerm.frame.size.height,v_alerm.frame.size.width, v_alerm.frame.size.height)];
        } completion:^(BOOL finished) {
            if (finished) {
                [self.view sendSubviewToBack:v_alerm];
                [v_alerm setHidden:YES];
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)TryAgainButtonAction:(UIButton *)sender {
    @try {
        [TryAgainView setHidden:YES];
        [self.view sendSubviewToBack:TryAgainView];
        [LoadingView setHidden:NO];
        [LoadingActivityIndicator startAnimating];
        [self.view bringSubviewToFront:LoadingView];
        if (InternetConnection) {
            [NSThread detachNewThreadSelector:@selector(GetSchedual) toTarget:self withObject:nil];
        }
        else{
            id result=  [self GetCashedShows];
            [self HandleCashShowsResult:result];
            if (result) {
                if ([result count]==0 ) {
                    [LoadingActivityIndicator stopAnimating];
                    [self.view sendSubviewToBack:LoadingView];
                    [LoadingView setHidden:YES];
                    
                    [TryAgainView setHidden:NO];
                    [self.view bringSubviewToFront:TryAgainView];
                }
            }
            else{
                [LoadingActivityIndicator stopAnimating];
                [self.view sendSubviewToBack:LoadingView];
                [LoadingView setHidden:YES];
                
                [TryAgainView setHidden:NO];
                [self.view bringSubviewToFront:TryAgainView];
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uipickerview datasource methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  [timeList count];
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",[timeList objectAtIndex:row]];
    
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedAlermValue = [[timeList objectAtIndex:row]intValue];
}
@end
