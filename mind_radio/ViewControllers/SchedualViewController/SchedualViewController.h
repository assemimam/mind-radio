//
//  SchedualViewController.h
//  kau
//
//  Created by Assem Imam on 7/15/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface SchedualViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    IBOutlet UIView *TryAgainView;
    __weak IBOutlet UIView *NoDataView;
    __weak IBOutlet UIPickerView *TimePicker;
    IBOutlet UIView *v_alerm;
    __weak IBOutlet UIButton *NextButton;
    __weak IBOutlet UIButton *PreviousButton;
    __weak IBOutlet UILabel *DayNameLabel;
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet UILabel *LoadingLabel;
    __weak IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
    __weak IBOutlet UITableView *SchedualTableView;
}
- (IBAction)PreviousButtonAction:(UIButton *)sender;
- (IBAction)NextButtonAction:(UIButton *)sender;
- (IBAction)AlermCancelButtonAction:(UIBarButtonItem *)sender;
- (IBAction)AlermSetButtonAction:(UIBarButtonItem *)sender;
- (IBAction)TryAgainButtonAction:(UIButton *)sender;


@end
