//
//  AboutViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MasterViewController.h"
#import "TTTAttributedLabel.h"

@interface AboutViewController : MasterViewController
{
    
    IBOutlet UIView *RetryView;
    __weak IBOutlet TTTAttributedLabel *Aboutlabel;
    __weak IBOutlet UIScrollView *ContainerScrollView;
}
- (IBAction)RetryButtonAction:(UIButton *)sender;
@end
