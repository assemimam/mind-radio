//
//  AboutViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
{
    NSString *aboutText;
}
@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
     Aboutlabel.font= [UIFont fontWithName:@"Arimo" size:Aboutlabel.font.pointSize];
    Aboutlabel.lineSpacing = 10;
    if (InternetConnection) {
        [NSThread detachNewThreadSelector:@selector(GetAboutData) toTarget:self withObject:nil];
    }
    else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"about"]) {
             Aboutlabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"about"];
            [Aboutlabel sizeToFit];
            [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, Aboutlabel.frame.origin.y+ Aboutlabel.frame.size.height + 10)];
        }
        else{
            [RetryView setHidden:NO];
            [self.view bringSubviewToFront:RetryView];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)HandleGetAboutDataResult
{
    @try {
        if (aboutText) {
            Aboutlabel.text = aboutText;
            [[NSUserDefaults standardUserDefaults] setObject:aboutText forKey:@"about"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [Aboutlabel sizeToFit];
            [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, Aboutlabel.frame.origin.y+ Aboutlabel.frame.size.height + 10)];
            
            [RetryView setHidden:YES];
            [self.view sendSubviewToBack:RetryView];
        }
        else{
            [RetryView setHidden:NO];
            [self.view bringSubviewToFront:RetryView];
        }
       
    }
    @catch (NSException *exception) {
        
    }
}
-(void)GetAboutData
{
    @try {
       id  result  = [Webservice GetAbout];
        if ([NSNull null]!= result&& ![result isEqualToString:@"null"]) {
            aboutText = result;
            aboutText =
            [aboutText stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        }
        else{
            aboutText=@"";
        }
       
        [self performSelectorOnMainThread:@selector(HandleGetAboutDataResult) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
   
}
- (IBAction)RetryButtonAction:(UIButton *)sender {
    @try {
        if (InternetConnection) {
            [NSThread detachNewThreadSelector:@selector(GetAboutData) toTarget:self withObject:nil];
        }
        else{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"about"]) {
                Aboutlabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"about"];
                [Aboutlabel sizeToFit];
                [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, Aboutlabel.frame.origin.y+ Aboutlabel.frame.size.height + 10)];
            }
            else{
                [RetryView setHidden:NO];
                [self.view bringSubviewToFront:RetryView];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
   
}
@end
