//
//  HostDetailsViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MasterViewController.h"
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>
@interface HostDetailsViewController : UIViewController
{
    __weak IBOutlet UIView *ImageContainerView;
    __weak IBOutlet FXBlurView *HeaderView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
    __weak IBOutlet UIScrollView *ContainerScrollView;
    __weak IBOutlet UILabel *HoppiesDescriptionLabel;
    __weak IBOutlet UILabel *HoppiesTitleLabel;
    __weak IBOutlet UILabel *BioDescriptionLabel;
    __weak IBOutlet UILabel *BioTitleLabel;
    __weak IBOutlet UILabel *HostNameLabel;
    __weak IBOutlet UIImageView *HostImageView;
    __weak IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
}
@property(nonatomic,retain)id HostDetails;
- (IBAction)BackAction:(UIButton *)sender;
@end
