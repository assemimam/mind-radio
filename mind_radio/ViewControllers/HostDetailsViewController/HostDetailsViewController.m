//
//  HostDetailsViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/28/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "HostDetailsViewController.h"
#import "TTTAttributedLabel.h"

@interface HostDetailsViewController ()
{
    NSMutableArray *HostInformationArray;
}
@end

@implementation HostDetailsViewController
@synthesize HostDetails;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    HeaderView.blurRadius = 5;
    NSString *imageUrlString =[[self.HostDetails objectForKey:@"ThumbnailURL"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
    NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
    if (!InternetConnection) {
         [LoadingActivityIndicator startAnimating];
        NSString *path =[self.HostDetails objectForKey:@"ThumbnailURL"];
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
        NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
        long long fileSize = [fileSizeNumber longLongValue]/1024;
        if (fileSize<500) {
            NSData *imageData =[NSData dataWithContentsOfFile:path];
            if (imageData) {
                HostImageView.image = [UIImage imageWithData:imageData];
                imageData=nil;
            }
            else{
                HostImageView.image = [UIImage imageNamed:@"defaulthosts-ic.png"];
            }
        }
        else{
              HostImageView.image = [UIImage imageNamed:@"defaulthosts-ic.png"];
        }
         [LoadingActivityIndicator stopAnimating];
    }
    else{
        if (imageUrl) {
            [LoadingActivityIndicator startAnimating];
            [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                if (image) {
                    HostImageView.image = image;
                    // HostImageView.image = [UIImage imageNamed:@"defaulthosts-ic.png"];
                }
                else{
                    HostImageView.image = [UIImage imageNamed:@"defaulthosts-ic.png"];
                }
                [LoadingActivityIndicator stopAnimating];
            }];
            
        }
    }
    TitleLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:TitleLabel.font.pointSize];
    BackButton.titleLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:BackButton.titleLabel.font.pointSize];
    HostNameLabel.font = [UIFont fontWithName:@"Arimo-Bold" size:HostNameLabel.font.pointSize];
    HostNameLabel.text = [NSNull null]!=[self.HostDetails objectForKey:@"Name"] ?[self.HostDetails objectForKey:@"Name"]:@"";
    
    UIFont *titleFont =[UIFont fontWithName:@"Arimo-Bold" size:20];
    UIFont *subtitleFont =[UIFont fontWithName:@"Arimo" size:18];
    UIColor *titleTextColor = [UIColor blackColor];
    UIColor *subtitleTextColor = [UIColor colorWithRed:109.0f/255.0f green:111.0f/255.0f blue:114.0f/255.0f alpha:1];
    int marginValue=5;
    int labelWidth =ContainerScrollView.frame.size.width -2*marginValue;
    
    int accumlatedYPosition =  ImageContainerView.frame.size.height+ImageContainerView.frame.origin.y + 20;
    if ([NSNull null]!= [self.HostDetails objectForKey:@"Details"] && ![[self.HostDetails objectForKey:@"Details"] isEqualToString:@"null"]&&[self.HostDetails objectForKey:@"Details"] ) {
        UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, 30)];
        titleLabel.font = titleFont;
        titleLabel.text =@"Details";
        titleLabel.textColor = titleTextColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        [ContainerScrollView addSubview:titleLabel];
        accumlatedYPosition+=30;
        
        TTTAttributedLabel*subtitleLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, labelWidth, 20)];
        subtitleLabel.numberOfLines = 0;
        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleLabel.font = subtitleFont;
        subtitleLabel.textColor = subtitleTextColor;
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.lineSpacing=10;
        subtitleLabel.text =[self.HostDetails objectForKey:@"Details"];
        [subtitleLabel sizeToFit];
        [subtitleLabel setFrame:CGRectMake(5, accumlatedYPosition, labelWidth, subtitleLabel.frame.size.height)];
        [ContainerScrollView addSubview:subtitleLabel];
        accumlatedYPosition=subtitleLabel.frame.size.height+subtitleLabel.frame.origin.y + 20;
    }
    if ([NSNull null]!= [self.HostDetails objectForKey:@"Bio"] && ![[self.HostDetails objectForKey:@"Bio"] isEqualToString:@"null"]&&[self.HostDetails objectForKey:@"Bio"] ) {
        UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, 30)];
        titleLabel.font = titleFont;
        titleLabel.text =@"Bio";
        titleLabel.textColor = titleTextColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        [ContainerScrollView addSubview:titleLabel];
        accumlatedYPosition+=30;
        
        TTTAttributedLabel*subtitleLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(marginValue, 0, labelWidth, 20)];
        subtitleLabel.numberOfLines = 0;
        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleLabel.font = subtitleFont;
        subtitleLabel.lineSpacing=10;
        subtitleLabel.textColor = subtitleTextColor;
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.text =[self.HostDetails objectForKey:@"Bio"];
        [subtitleLabel sizeToFit];
        [subtitleLabel setFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, subtitleLabel.frame.size.height)];
        [ContainerScrollView addSubview:subtitleLabel];
        accumlatedYPosition=subtitleLabel.frame.size.height+subtitleLabel.frame.origin.y + 20;

    }
    if ([NSNull null]!= [self.HostDetails objectForKey:@"HobbiesAndInterests"] && ![[self.HostDetails objectForKey:@"HobbiesAndInterests"] isEqualToString:@"null"]&&[self.HostDetails objectForKey:@"HobbiesAndInterests"] ) {
        UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, 30)];
        titleLabel.font = titleFont;
        titleLabel.text =@"Hobbies And Interests";
        titleLabel.textColor = titleTextColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        [ContainerScrollView addSubview:titleLabel];
        accumlatedYPosition+=30;
        
        TTTAttributedLabel*subtitleLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(marginValue, 0, labelWidth, 20)];
        subtitleLabel.numberOfLines = 0;
        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleLabel.font = subtitleFont;
        subtitleLabel.lineSpacing=10;
        subtitleLabel.textColor = subtitleTextColor;
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.text =[self.HostDetails objectForKey:@"HobbiesAndInterests"];
        [subtitleLabel sizeToFit];
        [subtitleLabel setFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, subtitleLabel.frame.size.height)];
        [ContainerScrollView addSubview:subtitleLabel];
        accumlatedYPosition=subtitleLabel.frame.size.height+subtitleLabel.frame.origin.y + 20;

    }
    if ([NSNull null]!= [self.HostDetails objectForKey:@"WhatHeDoes"] && ![[self.HostDetails objectForKey:@"WhatHeDoes"] isEqualToString:@"null"]&&[self.HostDetails objectForKey:@"WhatHeDoes"] ) {
        UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, 30)];
        titleLabel.font = titleFont;
        titleLabel.text =@"What He Does";
        titleLabel.textColor = titleTextColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        [ContainerScrollView addSubview:titleLabel];
        accumlatedYPosition+=30;
        
        TTTAttributedLabel*subtitleLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(marginValue, 0, labelWidth, 20)];
        subtitleLabel.numberOfLines = 0;
        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleLabel.font = subtitleFont;
        subtitleLabel.textColor = subtitleTextColor;
        subtitleLabel.lineSpacing=10;
        subtitleLabel.backgroundColor = [UIColor clearColor];
        subtitleLabel.text =[self.HostDetails objectForKey:@"WhatHeDoes"];
        [subtitleLabel sizeToFit];
        [subtitleLabel setFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, subtitleLabel.frame.size.height)];
        [ContainerScrollView addSubview:subtitleLabel];
        accumlatedYPosition=subtitleLabel.frame.size.height+subtitleLabel.frame.origin.y + 20;

    }
    if ([NSNull null]!= [self.HostDetails objectForKey:@"Contact"] && ![[self.HostDetails objectForKey:@"Contact"] isEqualToString:@"null"]&&[self.HostDetails objectForKey:@"Contact"] ) {
        UILabel*titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, accumlatedYPosition, 310, 30)];
        titleLabel.font = titleFont;
        titleLabel.text =@"Contact";
        titleLabel.textColor = titleTextColor;
        titleLabel.backgroundColor = [UIColor clearColor];
        [ContainerScrollView addSubview:titleLabel];
        accumlatedYPosition+=30;
        
        UITextView*subtitleTextView = [[UITextView alloc]initWithFrame:CGRectMake(marginValue, 0, labelWidth, 20)];
//        subtitleLabel.numberOfLines = 0;
//        subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subtitleTextView.font = subtitleFont;
        subtitleTextView.text =[self.HostDetails objectForKey:@"Contact"];
        [subtitleTextView setEditable:NO];
        [subtitleTextView setDataDetectorTypes:UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink|UIDataDetectorTypeAddress ];
        [subtitleTextView sizeToFit];
        subtitleTextView.textColor = subtitleTextColor;
        subtitleTextView.backgroundColor = [UIColor clearColor];
        [subtitleTextView setFrame:CGRectMake(marginValue, accumlatedYPosition, labelWidth, subtitleTextView.frame.size.height)];
        [ContainerScrollView addSubview:subtitleTextView];
        accumlatedYPosition=subtitleTextView.frame.size.height+subtitleTextView.frame.origin.y + 20;

    }
   
    [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, accumlatedYPosition )];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackAction:(UIButton *)sender {
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
