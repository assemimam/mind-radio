//
//  MyAlertsViewController.m
//  mind_radio
//
//  Created by Assem Imam on 11/6/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MyAlertsViewController.h"
#import "SchedualCell.h"

@interface MyAlertsViewController ()
{
    NSMutableArray *alertList;
}
@end

@implementation MyAlertsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        [self setNeedsStatusBarAppearanceUpdate];
        alertList = [[NSMutableArray alloc]init];
        
        [[Cashing getObject] setDatabase:@"mind_radio"];
        
        DB_Field  *fldShowId = [[DB_Field alloc]init];
        fldShowId.FieldName=@"my_alerts.ShowId";
        fldShowId.FieldAlias=@"ShowId";
        
        DB_Field  *fldDayOfWeek= [[DB_Field alloc]init];
        fldDayOfWeek.FieldName=@"DayOfWeek";
        
        DB_Field  *fldShowTime= [[DB_Field alloc]init];
        fldShowTime.FieldName=@"ShowTime";
        
        DB_Field  *fldThumbNailURL = [[DB_Field alloc]init];
        fldThumbNailURL.FieldName=@"ThumbNailURL";
        
        DB_Field  *fldTitle = [[DB_Field alloc]init];
        fldTitle.FieldName=@"Title";
        
        DB_Field  *fldInsertDate= [[DB_Field alloc]init];
        fldInsertDate.FieldName=@"InsertDate";
        
        id showsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldShowId,fldShowTime,fldInsertDate,fldThumbNailURL,fldTitle,fldDayOfWeek,nil] Tables:[NSArray arrayWithObjects:@"my_alerts",@"shows", nil] Where:@"my_alerts.ShowId = shows.ShowId" FromIndex:nil ToIndex:nil OrderByField:@"InsertDate" AscendingOrder:NO];
        
        if ([showsResut count]>0) {
            for (id show in showsResut) {
                NSMutableDictionary *showsDic = [show mutableCopy];
                DB_Field  *fldHostId = [[DB_Field alloc]init];
                fldHostId.FieldName=@"id";

                DB_Field  *fldHostName = [[DB_Field alloc]init];
                fldHostName.FieldName=@"Name";
                
                id hostsResut = [[Cashing getObject]getDataFromCashWithSelectColumns:[NSArray arrayWithObjects:fldHostId,fldHostName,nil] Tables:[NSArray arrayWithObject:@"show_hosts"] Where:[NSString stringWithFormat:@"ShowId=%@",[show objectForKey:@"ShowId"]] FromIndex:nil ToIndex:nil OrderByField:nil AscendingOrder:YES];
                if (hostsResut) {
                    [showsDic setObject:hostsResut forKey:@"Hosts"];
                }
                
                [alertList addObject:showsDic];
            }
        }
        else{
            [MyAlertTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }
        

    }
    @catch (NSException *exception) {
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)DeleteAllButtonAction:(UIButton *)sender {
    @try {
        for (id alert in alertList) {
            [[Cashing getObject] setDatabase:@"mind_radio"];
            [[Cashing getObject]DeleteDataFromTable:@"my_alerts" WhereCondition:[NSString stringWithFormat:@"ShowId=%i",[[alert objectForKey:@"ShowId"]intValue]]];
        }
        [alertList removeAllObjects];
        [MyAlertTableView reloadData];
        [MyAlertTableView setHidden:YES];
        [NoDataView setHidden:NO];
        [self.view bringSubviewToFront:NoDataView];
        [(AppDelegate*) [[UIApplication sharedApplication] delegate] ScheduleShowAlerts];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    @try {
        
        if ([alertList count]==0) {
            [MyAlertTableView setHidden:YES];
            [NoDataView setHidden:NO];
            [self.view bringSubviewToFront:NoDataView];
        }
        else{
            [MyAlertTableView setHidden:NO];
            [NoDataView setHidden:YES];
            [self.view sendSubviewToBack:NoDataView];

            NSIndexPath *cellIndexPath = [MyAlertTableView indexPathForCell:cell];
            [[Cashing getObject] setDatabase:@"mind_radio"];
            [[Cashing getObject]DeleteDataFromTable:@"my_alerts" WhereCondition:[NSString stringWithFormat:@"ShowId=%i",[[[alertList objectAtIndex:cellIndexPath.row]objectForKey:@"ShowId"]intValue]]];
            [alertList removeObjectAtIndex:cellIndexPath.row];
            [MyAlertTableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [(AppDelegate*) [[UIApplication sharedApplication] delegate] ScheduleShowAlerts];
            
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Remove"];
    
    return rightUtilityButtons;
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SchedualCell*cell;
    @try {
        
        cell =(SchedualCell*)[tableView dequeueReusableCellWithIdentifier:@"cleSchedual"];
        if (cell==nil) {
            cell = (SchedualCell*)[[[NSBundle mainBundle]loadNibNamed:@"SchedualCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.rightUtilityButtons = [self rightButtons];
        cell.delegate = self;


        [cell.LoadingActivityIndicator startAnimating];
        NSString *path =[[alertList objectAtIndex:indexPath.row]objectForKey:@"ThumbNailURL"];
            
        NSData *imageData =[NSData dataWithContentsOfFile:path];
        if (imageData) {
                cell.ShowImageView.image = [UIImage imageWithData:imageData];
        }
        else{
                cell.ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
        }
        [cell.LoadingActivityIndicator stopAnimating];
            
        
        [cell.AlertButton  setHidden:YES];

        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        dateFormatter.timeZone=[NSTimeZone localTimeZone];
        dateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSDateFormatter *fullDateFormatter =[[NSDateFormatter alloc]init];
        [fullDateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        fullDateFormatter.timeZone=[NSTimeZone localTimeZone];
        fullDateFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSString *convertedShowStartTime= [[alertList objectAtIndex:indexPath.row]objectForKey:@"ShowTime"];
        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
        [timeFormatter setDateFormat:@"h:mm a"];
        timeFormatter.timeZone=[NSTimeZone localTimeZone];
        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        NSString *currentDateString = [NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:[NSDate date]],convertedShowStartTime];
        
        NSString*showTime = [timeFormatter stringFromDate:[fullDateFormatter dateFromString:currentDateString]];
        cell.ProgramTimeLabel.text =[ NSString stringWithFormat:@"%@ %@",([[alertList objectAtIndex:indexPath.row] objectForKey:@"DayOfWeek"] && [NSNull null]!=[[alertList objectAtIndex:indexPath.row] objectForKey:@"DayOfWeek"])?[[alertList objectAtIndex:indexPath.row] objectForKey:@"DayOfWeek"]:@""
        ,showTime ];
        
        cell.ProgramNameLabel.text = [[alertList objectAtIndex:indexPath.row]objectForKey:@"Title"];
        cell.ProgramNameLabel.font =[UIFont fontWithName:@"Arimo-Bold" size:cell.ProgramNameLabel.font.pointSize];
        cell.ProgramTimeLabel.font =[UIFont fontWithName:@"Arimo-Bold" size:cell.ProgramTimeLabel.font.pointSize];
        cell.SpeakersLabel.font =[UIFont fontWithName:@"Arimo" size:cell.SpeakersLabel.font.pointSize];
        
        NSString *hostsString=@"";
        for (id host in  [[alertList objectAtIndex:indexPath.row]objectForKey:@"Hosts"]) {
            if ([host objectForKey:@"Name"] && [NSNull null]!=[host objectForKey:@"Name"] && ![[host objectForKey:@"Name"] isEqualToString:@"(null)"]) {
                if ([[host objectForKey:@"Name"] stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
                    hostsString =[hostsString stringByAppendingFormat:@"%@,",[host objectForKey:@"Name"]];
                }
            }
        }
        hostsString = [hostsString substringToIndex:[hostsString length]-1];
        cell.SpeakersLabel.text = hostsString;
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [alertList count];
}
#pragma -mark uitableview delegate methods
@end
