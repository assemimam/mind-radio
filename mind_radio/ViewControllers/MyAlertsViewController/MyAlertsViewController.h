//
//  MyAlertsViewController.h
//  mind_radio
//
//  Created by Assem Imam on 11/6/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import "SWTableViewCell.h"

@interface MyAlertsViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>
{
    IBOutlet UIView *NoDataView;
    __weak IBOutlet UITableView *MyAlertTableView;
}
- (IBAction)DeleteAllButtonAction:(UIButton *)sender;
@end
