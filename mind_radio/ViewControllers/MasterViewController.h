//
//  MasterViewController.h
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibrariesHeader.h"
@interface MasterViewController : UIViewController
{
    __weak IBOutlet UILabel *TitleLabel;
}
- (IBAction)MenuButtonAction:(UIButton *)sender;
- (IBAction)BackAction:(UIButton *)sender;
- (NSString *)ConvertToDeviceTime:(id)time AndTimeOffset:(float)timeOffset;
@end
