//
//  HostView.h
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HostView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *HostImageView;
@property (weak, nonatomic) IBOutlet UILabel *HostNameLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;

@end
