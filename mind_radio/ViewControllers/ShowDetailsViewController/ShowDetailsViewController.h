//
//  ShowDetailsViewController.h
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "MasterViewController.h"
#import "TTTAttributedLabel.h"

@interface ShowDetailsViewController : UIViewController
{
    
    __weak IBOutlet UIScrollView *ContainerScrollView;
    __weak IBOutlet UIActivityIndicatorView *LoadingActivityControl;
    __weak IBOutlet UIView *HostsContainerView;
    __weak IBOutlet UILabel *HostsTitleLabel;
    __weak IBOutlet TTTAttributedLabel *DescriptionLabel;
    __weak IBOutlet UILabel *DescriptionTitleLabel;
    __weak IBOutlet UILabel *ShowNamelabel;
    __weak IBOutlet UIImageView *ShowImageView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UILabel *TitleLabel;
}
@property(nonatomic)BOOL comeFromCash;
@property(nonatomic,retain)id ShowDetails;
- (IBAction)BackAction:(UIButton *)sender;
@end
