//
//  ShowDetailsViewController.m
//  mind_radio
//
//  Created by Assem Imam on 10/29/14.
//  Copyright (c) 2014 assem. All rights reserved.
//

#import "ShowDetailsViewController.h"
#import "HostView.h"
#import "QuartzCore/QuartzCore.h"
@interface ShowDetailsViewController ()

@end

@implementation ShowDetailsViewController
@synthesize ShowDetails,comeFromCash;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        [self setNeedsStatusBarAppearanceUpdate];
        TitleLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:TitleLabel.font.pointSize];
        ShowNamelabel.font= [UIFont fontWithName:@"Arimo-Bold" size:ShowNamelabel.font.pointSize];
        DescriptionTitleLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:DescriptionTitleLabel.font.pointSize];
        DescriptionLabel.font= [UIFont fontWithName:@"Arimo" size:DescriptionLabel.font.pointSize];
        DescriptionLabel.lineSpacing=10;
        HostsTitleLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:HostsTitleLabel.font.pointSize];
        BackButton.titleLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:BackButton.titleLabel.font.pointSize];
        NSString *imageUrlString = ([NSNull null]!=[self.ShowDetails objectForKey:@"ThumbNailURL"]&&[self.ShowDetails objectForKey:@"ThumbNailURL"]) ?[self.ShowDetails objectForKey:@"ThumbNailURL"]:nil;
        
        NSURL *imageUrl = [NSURL URLWithString:[imageUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        if (comeFromCash) {
            imageUrl = [NSURL fileURLWithPath:[self.ShowDetails objectForKey:@"ThumbNailURL"]];
            [LoadingActivityControl startAnimating];
            NSString *path =[self.ShowDetails objectForKey:@"ThumbNailURL"];
            
            NSData *imageData =[NSData dataWithContentsOfFile:path];
            if (imageData) {
                ShowImageView.image = [UIImage imageWithData:imageData];
            }
            else{
                ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
            }
            [LoadingActivityControl stopAnimating];
        }
        if (imageUrl) {
            [LoadingActivityControl startAnimating];
            [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                if (image) {
                    ShowImageView.image = image;
                    // HostImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
                }
                else{
                    ShowImageView.image = [UIImage imageNamed:@"square-defaultic.png"];
                }
                [LoadingActivityControl stopAnimating];
            }];
        }
        
        ShowNamelabel.text = ([NSNull null]!=[self.ShowDetails objectForKey:@"Title"]&&[self.ShowDetails objectForKey:@"Title"]) ?[self.ShowDetails objectForKey:@"Title"]:nil;
        DescriptionLabel.text = ([NSNull null]!=[self.ShowDetails objectForKey:@"Description"]&&[self.ShowDetails objectForKey:@"Description"]) ?[self.ShowDetails objectForKey:@"Description"]:nil;
        [DescriptionLabel sizeToFit];
        [HostsTitleLabel setFrame: CGRectMake(HostsTitleLabel.frame.origin.x, DescriptionLabel.frame.origin.y + DescriptionLabel.frame.size.height + 10, HostsTitleLabel.frame.size.width, HostsTitleLabel.frame.size.height)];
        [HostsContainerView setFrame: CGRectMake(HostsContainerView.frame.origin.x, HostsTitleLabel.frame.origin.y + HostsTitleLabel.frame.size.height , HostsContainerView.frame.size.width, HostsContainerView.frame.size.height)];
        
        //Drow Hosts Views
        int X=0;
        int Y=0;
        int index=0;
        if ([NSNull null]!=[self.ShowDetails objectForKey:@"Hosts"]&&[self.ShowDetails objectForKey:@"Hosts"] ) {
            if ([[self.ShowDetails objectForKey:@"Hosts"] count]==0) {
                HostView *v_first_host = (HostView*)[[[NSBundle mainBundle]loadNibNamed:@"HostView" owner:nil options:nil]objectAtIndex:0];
                v_first_host.HostNameLabel.text = @"";
                v_first_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                v_first_host.frame = CGRectMake(X, Y, v_first_host.frame.size.width, v_first_host.frame.size.height);
                [HostsContainerView addSubview:v_first_host];
                X+=X+v_first_host.frame.size.width;
                
                HostView *v_seconed_host = (HostView*)[[[NSBundle mainBundle]loadNibNamed:@"HostView" owner:nil options:nil]objectAtIndex:0];
                v_seconed_host.HostNameLabel.text = @"";
                v_seconed_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                v_seconed_host.frame = CGRectMake(X, Y, v_seconed_host.frame.size.width, v_seconed_host.frame.size.height);
                [HostsContainerView addSubview:v_seconed_host];
                X+=X+v_seconed_host.frame.size.width;
                [HostsContainerView setFrame:CGRectMake(HostsContainerView.frame.origin.x, HostsContainerView.frame.origin.y, HostsContainerView.frame.size.width, 151)];
                
            }
            else{
                for (id host in [self.ShowDetails objectForKey:@"Hosts"]) {
                    
                    HostView *v_host = (HostView*)[[[NSBundle mainBundle]loadNibNamed:@"HostView" owner:nil options:nil]objectAtIndex:0];
                    v_host.HostNameLabel.font =[UIFont fontWithName:@"Arimo-Bold" size:v_host.HostNameLabel.font.pointSize];
                    NSString *imageUrlString =[host objectForKey:@"ThumbnailURL"] && [NSNull null]!=[host objectForKey:@"ThumbnailURL"] ?[[host objectForKey:@"ThumbnailURL"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:nil ;
                    NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
                    if (comeFromCash) {
                        imageUrl = [NSURL fileURLWithPath:[host objectForKey:@"ThumbnailURL"]];
                        [v_host.LoadingActivityIndicator startAnimating];
                        NSString *path =imageUrlString;
                        NSData *imageData =[NSData dataWithContentsOfFile:path];
                        if (imageData) {
                             v_host.HostImageView.image= [UIImage imageWithData:imageData];
                        }
                        else{
                             v_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                        }
                        [v_host.LoadingActivityIndicator stopAnimating];
                    }
                    if (imageUrl) {
                        [v_host.LoadingActivityIndicator startAnimating];
                        [[ImageCache sharedInstance] downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                            if (image) {
                                v_host.HostImageView.image = image;
                                //v_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                            }
                            else{
                                v_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                            }
                            [v_host.LoadingActivityIndicator stopAnimating];
                        }];
                    }
                    else{
                        v_host.HostImageView.image = [UIImage imageNamed:@"defaultcircle-ic.png"];
                    }
                    
                    v_host.HostImageView.layer.cornerRadius = v_host.HostImageView.frame.size.width / 2;
                    v_host.HostImageView.clipsToBounds = YES;
                    if (index>0) {
                        if (index%2 == 0) {
                            Y+= v_host.frame.size.height+2;
                            X=0;
                        }
                    }
                    v_host.HostNameLabel.text = [host objectForKey:@"Name"]&&[NSNull null]!= [host objectForKey:@"Name"]?[host objectForKey:@"Name"]:@"";
                    v_host.frame = CGRectMake(X, Y, v_host.frame.size.width, v_host.frame.size.height);
                    [HostsContainerView addSubview:v_host];
                    X+=X+v_host.frame.size.width;
                   
                    index++;
                    
                }
                [HostsContainerView setFrame:CGRectMake(HostsContainerView.frame.origin.x, HostsContainerView.frame.origin.y, HostsContainerView.frame.size.width, Y+151)];
            }
            
        }
        else{
            
        }
        [ContainerScrollView setContentSize:CGSizeMake(ContainerScrollView.frame.size.width, HostsContainerView.frame.size.height + HostsContainerView.frame.origin.y)];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)setRoundedView:(UIImageView *)roundedView toDiameter:(float)newSize;
{
    @try {
        CGPoint saveCenter = roundedView.center;
        CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
        roundedView.frame = newFrame;
        roundedView.layer.cornerRadius = newSize / 2.0;
        roundedView.center = saveCenter;
    }
    @catch (NSException *exception) {
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackAction:(UIButton *)sender {
    @try {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        
    }
    
}
@end
