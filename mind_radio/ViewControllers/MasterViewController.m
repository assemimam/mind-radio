//
//  MasterViewController.m
//  SFDA_GIS
//
//  Created by Assem Imam on 8/5/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MasterViewController.h"
#import "MFSideMenu.h"
@interface MasterViewController ()
{
    BOOL *showSidemenu;
}
@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    TitleLabel.font= [UIFont fontWithName:@"Arimo-Bold" size:TitleLabel.font.pointSize];
    [self.menuContainerViewController setPaningEnabled:NO];

}
- (NSString *)ConvertFromDeviceTime:(id)time AndTimeOffset:(float)timeOffset
{
    NSString *showTime;
    @try {
        time =[NSNull null]!= time?time:@"";
        float timeZoneSeconds=timeOffset*3600.0f;
        
        NSTimeZone* deviceTimeZone = [NSTimeZone localTimeZone];
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:timeZoneSeconds];
        
        NSDateFormatter *serverTimeFormatter =[[NSDateFormatter alloc]init];
        [serverTimeFormatter setDateFormat:@"HH:mm:ss"];
        serverTimeFormatter.timeZone=deviceTimeZone;
        serverTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        
        NSDate *serverShowTime =[serverTimeFormatter dateFromString:time];
        
        NSInteger currentGMTOffset = [sourceTimeZone secondsFromGMTForDate:serverShowTime];
        NSTimeZone *convertedSourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset];
        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
        [timeFormatter setDateFormat:@"HH:mm:ss"];
        timeFormatter.timeZone=convertedSourceTimeZone;
        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        showTime =[timeFormatter stringFromDate:serverShowTime];
    }
    @catch (NSException *exception) {
        
    }

    return showTime;
}


- (NSString *)ConvertToDeviceTime:(id)time AndTimeOffset:(float)timeOffset
{
    NSString *showTime;
    @try {
        time =[NSNull null]!= time?time:@"";
        float timeZoneSeconds=timeOffset*3600.0f;
        
        NSTimeZone* deviceTimeZone = [NSTimeZone localTimeZone];
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:timeZoneSeconds];
        NSDateFormatter *serverTimeFormatter =[[NSDateFormatter alloc]init];
        [serverTimeFormatter setDateFormat:@"HH:mm:ss"];
        serverTimeFormatter.timeZone=sourceTimeZone;
        serverTimeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        NSDate *serverShowTime =[serverTimeFormatter dateFromString:time];
        
        NSInteger currentGMTOffset = [deviceTimeZone secondsFromGMTForDate:serverShowTime];
        NSTimeZone *convertedSourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset];
        NSDateFormatter *timeFormatter =[[NSDateFormatter alloc]init];
        [timeFormatter setDateFormat:@"HH:mm:ss"];
        timeFormatter.timeZone=convertedSourceTimeZone;
        timeFormatter.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        showTime =[timeFormatter stringFromDate:serverShowTime];
    }
    @catch (NSException *exception) {
        
    }
    return showTime;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark slidenavigation delegate
- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
  
    return NO;
}
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)MenuButtonAction:(UIButton *)sender {
    @try {
            [self.menuContainerViewController setMenuState:MFSideMenuStateLeftMenuOpen];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)BackAction:(UIButton *)sender {
    @try {
       
    }
    @catch (NSException *exception) {
        
    }
}


@end
